import { Component, OnInit } from "@angular/core";
import { navItems } from "../../_nav";
import { Router } from "@angular/router";
import { SharedService } from "../../shared/services/shared.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./default-layout.component.html",
})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems = navItems;
  userRole = "";
  constructor(private router: Router, private share: SharedService) {}

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
  logout() {
    if (this.userRole === "ROLE_ADMIN") {
      this.userRole = "";
      this.navItems.pop();
    }
    localStorage.clear();
    this.router.navigate(["/login"]);
  }

  ngOnInit(): void {
    this.userRole = this.share.getUserRole();
    this.navItems.push(
      {
        name: "location de vélos",
        icon: "icon-directions",
        variant: "success",
        children: [
          {
            name: "Calendrier",
            url: "/calendar",
            icon: "fa fa-calendar-check-o",
          },
          {
            name: "Liste des locations",
            url: "/reservation-list",
            icon: "fa fa-list-ul",
          },
        ],
      },
      {
        name: "Gestion des accées",
        url: "/gestion-des-utilisateurs",
        icon: "cil-user",
      }
    );
    if (this.userRole === "ROLE_ADMIN" && this.navItems.length < 4) {
    }
  }
}
