import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import apiUrl from "../config";
import { Observable } from "rxjs";
import { catchError, filter } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class SharedService {
  readonly APIUrl = apiUrl;
  constructor(private http: HttpClient) {}

  login(username: string, password: string) {
    return this.http.post(this.APIUrl + "/login", { username, password });
  }
  getToken() {
    return localStorage.getItem("token");
  }
  getUserRole() {
    return JSON.parse(localStorage.getItem("user")).roles[0];
  }
  loggedIn() {
    return !!(localStorage.getItem("token")&&localStorage.getItem("user"));
  }
  updateUserData(url: string, id, updatedValue) {
    url += "/";
    return this.http.patch(this.APIUrl + url + id, updatedValue);
  }
  getEntity(url: string, filters) {
    url += "?";
    filters.forEach((filter) => {
      let key = Object.keys(filter)[0];
      url += "&" + key + "=" + filter[key];
    });
    return this.http.get(this.APIUrl + url);
  }
  getOneEntity(url: string, id) {
    url += "/";
    return this.http.get(this.APIUrl + url + id);
  }
  updateEntity(url: string, id, updatedValue) {
    url += "/";
    return this.http.put(this.APIUrl + url + id, updatedValue);
  }
  addEntity(url: string, SiteType: any) {
    return this.http.post(this.APIUrl + url, SiteType);
  }

  deleteEntity(entity: any, val: any) {
    return this.http.delete(this.APIUrl + `/${entity}/` + val);
  }
  getEntityCount(entity: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + `/${entity}/count`);
  }
  getUser(){
    return this.http.get(this.APIUrl + '/me');
  }
}
