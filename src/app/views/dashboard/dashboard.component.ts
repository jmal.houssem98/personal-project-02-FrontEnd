import { Component, OnInit } from "@angular/core";
import { getStyle, hexToRgba } from "@coreui/coreui/dist/js/coreui-utilities";
import { CustomTooltips } from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import { SharedService } from "../../shared/services/shared.service";
import { LoaderService } from "../../shared/services/loader.service";
import { Router } from "@angular/router";

@Component({
  templateUrl: "dashboard.component.html",
})
export class DashboardComponent implements OnInit {
  constructor(
    private share: SharedService,
    public loaderService: LoaderService,
    private router: Router
  ) {}
  url = "/users";
  isAdmin: any;

  ngOnInit(): void {
    this.isAdmin = this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    this.countNotInitializedElements();
    this.countAccomodationPlaces();
    this.getAccomodations();
    this.countElementStates();
    this.getAllUsers();
    this.countUsers();
  }
  users = [];
  totalCount: number;
  totalCountActif: number;
  totalCountConfirmed: number;
  totalCountNotConfirmed: number;

  getAllUsers() {
    this.share.getEntity(this.url, []).subscribe((res) => {
      const total = res["hydra:totalItems"];
      this.totalCount = total;
      this.users = res["hydra:member"];
    });
  }
  countUsers() {
    this.share.getEntity(this.url, [{ is_active: "true" }]).subscribe((res) => {
      const totalActif = res["hydra:totalItems"];
      this.totalCountActif = totalActif;
    });
    this.share
      .getEntity(this.url, [{ confirmed_account: "true" }])
      .subscribe((res) => {
        const totalConfirmed = res["hydra:totalItems"];
        this.totalCountConfirmed = totalConfirmed;
      });
    this.share
      .getEntity(this.url, [{ confirmed_account: "false" }])
      .subscribe((res) => {
        const totalConfirmed = res["hydra:totalItems"];
        this.totalCountNotConfirmed = totalConfirmed;
      });
  }
  elementsTotalNumber = 0;
  elementsNotInitializedNumber = 0;
  showElementChart = false;

  countNotInitializedElements() {
    this.elementsTotalNumber = 0;
    this.elementsNotInitializedNumber = 0;
    this.showElementChart = false;
    this.share.getEntity("/elements", []).subscribe((result) => {
      this.elementsTotalNumber = result["hydra:totalItems"];
    });
    this.share
      .getEntity("/elements", [{ recognition_code: "" }])
      .subscribe((result) => {
        this.elementsNotInitializedNumber = result["hydra:totalItems"];
        this.pieChartData[0] = this.elementsTotalNumber;
        this.pieChartData[1] = this.elementsNotInitializedNumber;
        if (this.pieChartData[0] == 0) {
          this.delay(2000).then((any) => {
            //console.log("wating for results...");
            this.pieChartData[0] = this.elementsTotalNumber;
            this.pieChartData[1] = this.elementsNotInitializedNumber;
            this.showElementChart = true;
          });
        } else {
          this.showElementChart = true;
        }
      });
  }

  //accomodation chart calculate
  showAccomodationChart = false;
  partialNumber = 0;
  InitialisedNumber = 0;
  resultReady1 = false;
  resultReady2 = false;
  listReady = false;
  accommodationAll = [""];
  accommodationId: any;
  accommodationPartiallyInitialized = [""];
  accommodationTotalyInitialized = [""];

  getAccomodations() {
    this.showAccomodationChart = false;
    this.listReady = false;
    this.resultReady1 = false;
    this.resultReady2 = false;
    this.accommodationAll = [];
    this.accommodationPartiallyInitialized = [];
    this.accommodationTotalyInitialized = [];
    this.partialNumber = 0;
    this.InitialisedNumber = 0;
    this.share.getEntity("/elements", []).subscribe(
      (result) => {
        // @ts-ignore
        result["hydra:member"].map((item) => {
          this.accommodationId = item.accommodation["id"];
          if (this.accommodationAll.indexOf(this.accommodationId) === -1) {
            this.accommodationAll.push(this.accommodationId);
          }
        });
        this.resultReady1 = true;
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(["/login"]);
        }
        console.log(error);
      }
    );
    this.share.getEntity("/elements", [{ recognition_code: "" }]).subscribe(
      (result) => {
        // @ts-ignore
        result["hydra:member"].map((item) => {
          this.accommodationId = item.accommodation["id"];
          if (
            this.accommodationPartiallyInitialized.indexOf(
              this.accommodationId
            ) === -1
          ) {
            this.accommodationPartiallyInitialized.push(this.accommodationId);
          }
        });
        this.resultReady2 = true;
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(["/login"]);
        }
        console.log(error);
      }
    );
    this.delay(9000).then((any) => {
      if (this.resultReady1 && this.resultReady2) {
        for (const value of this.accommodationAll) {
          if (this.accommodationPartiallyInitialized.indexOf(value) === -1) {
            this.accommodationTotalyInitialized.push(value);
          }
        }
        //console.log("result ready !");
        this.listReady = true;
        this.InitialisedNumber = this.accommodationTotalyInitialized.length;
        this.partialNumber = this.accommodationPartiallyInitialized.length;
        this.doughnutChartData[1] =
          this.accommodationPartiallyInitialized.length;
        this.doughnutChartData[2] = this.accommodationTotalyInitialized.length;
        this.showAccomodationChart = true;
      } else {
        //console.log("must wait more --> Itératioon 2");
        this.delay(10000).then((any) => {
          if (this.resultReady1 && this.resultReady2) {
            for (const value of this.accommodationAll) {
              if (
                this.accommodationPartiallyInitialized.indexOf(value) === -1
              ) {
                this.accommodationTotalyInitialized.push(value);
              }
            }
          }
          //console.log("result ready 2!");
          this.listReady = true;
          this.InitialisedNumber = this.accommodationTotalyInitialized.length;
          this.partialNumber = this.accommodationPartiallyInitialized.length;
          this.doughnutChartData[1] =
            this.accommodationPartiallyInitialized.length;
          this.doughnutChartData[2] =
            this.accommodationTotalyInitialized.length;
          this.showAccomodationChart = true;
        });
      }
    });
  }
  mont = 0;
  repa = 0;
  rebut = 0;
  stock = 0;
  stateList = [];
  countElementStates() {
    this.mont = 0;
    this.repa = 0;
    this.rebut = 0;
    this.stock = 0;
    this.share.getEntity("/elements", []).subscribe(
      (result) => {
        // @ts-ignore
        result["hydra:member"].map((item) => {
          if (item.state.id == 1) {
            this.mont += 1;
          } else if (item.state.id == 2) {
            this.repa += 1;
          } else if (item.state.id == 3) {
            this.rebut += 1;
          } else if (item.state.id == 4) {
            this.stock += 1;
          }
        });
        this.stateList.push(this.stock, this.rebut, this.repa, this.mont);
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(["/login"]);
        }
        console.log(error);
      }
    );
  }

  bn = 0;
  me = 0;
  de = 0;
  ta = 0;
  placeList = [];
  countAccomodationPlaces() {
    this.bn = 0;
    this.me = 0;
    this.de = 0;
    this.ta = 0;
    this.share.getEntity("/accommodations", []).subscribe(
      (result) => {
        // @ts-ignore
        result["hydra:member"].map((item) => {
          if (item.place.id == 1) {
            this.de += 1;
          } else if (item.place.id == 2) {
            this.bn += 1;
          } else if (item.place.id == 3) {
            this.me += 1;
          } else if (item.place.id == 4) {
            this.ta += 1;
          }
        });
        this.placeList.push(this.de, this.bn, this.me, this.ta);
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(["/login"]);
        }
        console.log(error);
      }
    );
  }

  ///////////////////////////////////////////////////---> Charts here

  // lineChart
  public lineChartData: Array<any> = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: "Series A" },
    { data: [28, 48, 40, 19, 86, 27, 90], label: "Series B" },
    { data: [18, 48, 77, 9, 100, 27, 40], label: "Series C" },
  ];
  public lineChartLabels: Array<any> = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
  ];
  public lineChartOptions: any = {
    animation: false,
    responsive: true,
  };
  public lineChartColours: Array<any> = [
    {
      // grey
      backgroundColor: "rgba(148,159,177,0.2)",
      borderColor: "rgba(148,159,177,1)",
      pointBackgroundColor: "rgba(148,159,177,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(148,159,177,0.8)",
    },
    {
      // dark grey
      backgroundColor: "rgba(77,83,96,0.2)",
      borderColor: "rgba(77,83,96,1)",
      pointBackgroundColor: "rgba(77,83,96,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(77,83,96,1)",
    },
    {
      // grey
      backgroundColor: "rgba(148,159,177,0.2)",
      borderColor: "rgba(148,159,177,1)",
      pointBackgroundColor: "rgba(148,159,177,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(148,159,177,0.8)",
    },
  ];
  public lineChartLegend = true;
  public lineChartType = "line";

  // barChart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  public barChartLabels: string[] = [
    "En stock",
    "En rebut",
    "En réparation",
    "Monté",
  ];
  public barChartType = "bar";
  public barChartLegend = true;

  public barChartData: any[] = [
    { data: this.stateList, label: "Nombre d'éléments" },
  ];

  // barChart 2 !!!!
  public barChartOptions2: any = {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  public barChartLabels2: string[] = [
    "Djerba",
    "Bizerte",
    "Monastir",
    "Tataouine",
  ];
  public barChartType2 = "bar";
  public barChartLegend2 = true;

  public barChartData2: any[] = [
    { data: this.placeList, label: "Nombre d'hébergement" },
  ];

  // Doughnut
  public doughnutChartLabels: string[] = [
    "Hébergement non initialisé",
    "Hébergement partiellement initialisé",
    "Hébergement initialisé",
  ];
  public doughnutChartData: number[] = [0, 0, 0];
  public doughnutChartType = "doughnut";

  // Radar
  public radarChartLabels: string[] = [
    "Eating",
    "Drinking",
    "Sleeping",
    "Designing",
    "Coding",
    "Cycling",
    "Running",
  ];

  public radarChartData: any = [
    { data: [65, 59, 90, 81, 56, 55, 40], label: "Series A" },
    { data: [28, 48, 40, 19, 96, 27, 100], label: "Series B" },
  ];
  public radarChartType = "radar";

  // Pie
  public pieChartLabels: string[] = [
    "éléments initialisés",
    "éléments non initialisés",
  ];
  public pieChartData: number[] = [50, 50];
  public pieChartType = "pie";

  // PolarArea
  public polarAreaChartLabels: string[] = [
    "Download Sales",
    "In-Store Sales",
    "Mail Sales",
    "Telesales",
    "Corporate Sales",
  ];
  public polarAreaChartData: number[] = [300, 500, 100, 40, 120];
  public polarAreaLegend = true;

  public polarAreaChartType = "polarArea";

  // events
  public chartClicked(e: any): void {
    //console.log(e);
  }

  public chartHovered(e: any): void {
    //console.log(e);
  }
  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("fired")
    );
  }
}
