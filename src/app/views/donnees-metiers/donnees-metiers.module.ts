import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DonneesMetiersMRoutingModule } from "./donnees-metiers-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ModalModule } from "ngx-bootstrap/modal";
import { SiteComponent } from "./site/site.component";
import { TarificationComponent } from "./tarification/tarification.component";
import { ElementComponent } from "./element/element.component";
import { PretComponent } from "./pret/pret.component";
import { ReparationComponent } from "./reparation/reparation.component";
import { HebergementComponent } from './hebergement/hebergement.component';
import { LieuxComponent } from './lieux/lieux.component';
import { VersionComponent } from './version/version.component';
import { GammeComponent } from './gamme/gamme.component';

@NgModule({
  declarations: [
    SiteComponent,
    TarificationComponent,
    ElementComponent,
    PretComponent,
    ReparationComponent,
    HebergementComponent,
    LieuxComponent,
    VersionComponent,
    GammeComponent,
  ],
  imports: [
    CommonModule,
    DonneesMetiersMRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
  ],
})
export class DonneesMetiersModule {}
