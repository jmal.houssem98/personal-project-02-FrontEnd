import { Component, OnInit, ViewChild } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { Router } from "@angular/router";
import { LoaderService } from "../../../shared/services/loader.service";

@Component({
  selector: "app-tarification",
  templateUrl: "./tarification.component.html",
  styleUrls: [
    "./tarification.component.scss",
    "../donnees-metiers.module.scss",
  ],
})
export class TarificationComponent implements OnInit {
  url = "/pricings";
  rentalTypeSelected = null;
  inputStyle = {
    backgroundColor: "",
  };
  inputStyle2 = {
    backgroundColor: "",
  };
  isLoading = false;
  dataNotFound = false;
  emptyPick = false;
  entityEmpty = false;
  entityEmpty2 = false;
  user = {
    isAdmin: true,
  };
  filters = [
    { id: null },
    { page: 1 },
    { itemsPerPage: "10" },
    { "year%5Bgte%5D": "" },
    { "cost%5Bgte%5D": "" },
    { element_type: "" },
  ];
  filterId = false;
  filterNom = false;
  filterActif = false;
  filterElementType = false;
  filterCost = false;

  newtSiteType = {
    cost: null,
    year: null,
    elementType: "",
  };

  updateSiteType = {
    id: null,
    year: null,
    cost: null,
    elementType: "",
  };

  successAddSiteType = false;
  errorAddSiteType = false;
  successModifySiteType = false;
  failModifySiteType = false;
  siteTypes = [];
  elementTypes = [];

  statut = {
    isActive: null,
  };
  loading = true;
  pagination = {
    first: 1,
    prev: 1,
    current: 1,
    next: 1,
    last: 1,
  };
  totalCount: number;
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.getElementTypes();
    this.user.isAdmin =
      this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    this.getSiteTypes(1);
    this.share
      .getEntity(this.url, [{ page: 1 }, { itemsPerPage: "1" }])
      .subscribe((res) => {
        const total = res["hydra:totalItems"];
        this.totalCount = total;
      });
  }
  changeStatut(id, active: boolean) {
    this.statut.isActive = active ? false : true;
    this.share.updateEntity(this.url, id, this.statut).subscribe(
      (res) => {
        if (this.siteTypes.length == 1 && this.pagination.current > 1) {
          this.getSiteTypes(this.pagination.prev);
        } else {
          this.getSiteTypes(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
      }
    );
  }
  getSiteTypes(page: number) {
    this.dataNotFound = false;
    this.isLoading = true;
    this.filters[1].page = page;
    this.share.getEntity(this.url, this.filters).subscribe(
      (res) => {
        this.isLoading = false;
        this.siteTypes = res["hydra:member"];
        if (this.siteTypes.length == 0) {
          this.dataNotFound = true;
        } else {
          this.dataNotFound = false;
        }
        if (page == 1) {
          this.pagination.prev = 1;
        } else {
          this.pagination.prev = parseInt(
            res["hydra:view"]["hydra:previous"]?.split("=").reverse()[0]
          );
        }
        if (
          page ==
          parseInt(res["hydra:view"]["hydra:last"]?.split("=").reverse()[0])
        ) {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
          );
        } else {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:next"]?.split("=").reverse()[0]
          );
        }
        this.pagination.current = page;
        this.pagination.first = parseInt(
          res["hydra:view"]["hydra:first"]?.split("=").reverse()[0]
        );
        this.pagination.last = parseInt(
          res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
        );
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
        this.isLoading = false;
      }
    );
  }
  addNewSiteType() {
    if (this.newtSiteType.elementType == "") {
      this.emptyPick = true;
      return 0;
    }
    if (!this.entityEmpty) {
      this.share.addEntity(this.url, this.newtSiteType).subscribe(
        (res) => {
          this.emptyPick = false;
          this.getSiteTypes(this.pagination.current);
          this.newtSiteType = {
            cost: null,
            year: null,
            elementType: null,
          };
          this.errorAddSiteType = false;
          this.successAddSiteType = true;
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          }
          this.successAddSiteType = false;
          this.errorAddSiteType = true;
        }
      );
    }
  }
  resetAddModal() {
    this.newtSiteType = {
      cost: null,
      year: null,
      elementType: "",
    };
    this.successAddSiteType = false;
    this.errorAddSiteType = false;
    this.inputStyle.backgroundColor = "";
    this.entityEmpty = false;
  }
  showUpdateModal(siteType) {
    this.getElementTypes();
    this.updateSiteType = {
      id: siteType.id,
      year: siteType.year,
      cost: siteType.cost,
      elementType: siteType.element_type ? siteType.element_type["@id"] : "",
    };
  }
  putSiteType() {
    this.share
      .updateEntity(this.url, this.updateSiteType.id, this.updateSiteType)
      .subscribe(
        (res) => {
          this.getSiteTypes(this.pagination.current);
          this.successModifySiteType = true;
          this.delay(5000).then((any) => {
            this.successModifySiteType = false;
          });
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          } else if (error.status == 422) {
            this.failModifySiteType = true;
            this.delay(10000).then((any) => {
              this.failModifySiteType = false;
            });
          }
        }
      );
  }
  uniquenessCheck() {
    this.share
      .getEntity(this.url, [{ year: this.newtSiteType.year }])
      .subscribe((res) => {
        if (!this.newtSiteType.year) {
          this.inputStyle.backgroundColor = "#fee2e1";
          this.entityEmpty = true;
        } else {
          let exist = false;
          if (res["hydra:member"].length > 0) {
            res["hydra:member"].map((item) => {
              if (item.year === this.newtSiteType.year) {
                exist = true;
              }
            });
            if (exist) {
              this.inputStyle.backgroundColor = "#dbf2e3";
              this.entityEmpty = false;
            } else {
              this.inputStyle.backgroundColor = "#dbf2e3";
              this.entityEmpty = false;
            }
          } else {
            this.inputStyle.backgroundColor = "#dbf2e3";
            this.entityEmpty = false;
          }
        }
      });
  }

  uniquenessCheck2() {
    this.share
      .getEntity(this.url, [{ year: this.newtSiteType.cost }])
      .subscribe((res) => {
        if (!this.newtSiteType.cost) {
          this.inputStyle2.backgroundColor = "#fee2e1";
          this.entityEmpty2 = true;
        } else {
          let exist = false;
          if (res["hydra:member"].length > 0) {
            res["hydra:member"].map((item) => {
              if (item.cost === this.newtSiteType.cost) {
                exist = true;
              }
            });
            if (exist) {
              this.inputStyle2.backgroundColor = "#dbf2e3";
              this.entityEmpty = false;
            } else {
              this.inputStyle2.backgroundColor = "#dbf2e3";
              this.entityEmpty2 = false;
            }
          } else {
            this.inputStyle2.backgroundColor = "#dbf2e3";
            this.entityEmpty2 = false;
          }
        }
      });
  }

  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("fired")
    );
  }

  getElementTypes() {
    this.share
      .getEntity("/element_types", [{ is_active: "true" }])
      .subscribe((result) => {
        this.elementTypes = result["hydra:member"];
      });
  }
}
