import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ElementComponent } from "./element/element.component";
import { PretComponent } from "./pret/pret.component";
import { ReparationComponent } from "./reparation/reparation.component";
import { SiteComponent } from "./site/site.component";
import { TarificationComponent } from "./tarification/tarification.component";
import {HebergementComponent} from "./hebergement/hebergement.component";
import {LieuxComponent} from "./lieux/lieux.component";
import {GammeComponent} from "./gamme/gamme.component";
import {VersionComponent} from "./version/version.component";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "données-métiers",
    },
    children: [
      {
        path: "site",
        component: SiteComponent,
        data: {
          title: "site",
        },
      },
      {
        path: "hébergements",
        component: HebergementComponent,
        data: {
          title: "hébergement",
        },
      },
      {
        path: "lieux",
        component: LieuxComponent,
        data: {
          title: "lieux",
        },
      },
{
        path: "gammes",
        component: GammeComponent,
        data: {
          title: "gamme",
        },
      },
{
        path: "versions",
        component: VersionComponent,
        data: {
          title: "versions",
        },
      },

      {
        path: "tarification",
        component: TarificationComponent,
        data: {
          title: "tarification",
        },
      },
      {
        path: "reparation",
        component: ReparationComponent,
        data: {
          title: "reparation",
        },
      },
      {
        path: "pret",
        component: PretComponent,
        data: {
          title: "pret",
        },
      },
      {
        path: "element",
        component: ElementComponent,
        data: {
          title: "element",
        },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DonneesMetiersMRoutingModule {}
