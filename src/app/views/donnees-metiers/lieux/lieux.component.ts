import { Component, OnInit } from '@angular/core';
import {SharedService} from "../../../shared/services/shared.service";
import {Router} from "@angular/router";
import {LoaderService} from "../../../shared/services/loader.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-lieux',
  templateUrl: './lieux.component.html',
  styleUrls: ['../donnees-metiers.module.scss','./lieux.component.scss']
})
export class LieuxComponent implements OnInit{
  url = "/places";
  inputStyle = {
    backgroundColor: "",
  };
  addForm:NgForm
  isLoading = false;
  dataNotFound = false;
  entityExist = false;
  entityEmpty = false;
  user = {
    isAdmin: true,
  };
  filters = [
    { id: null },
    { page: 1 },
    { itemsPerPage: "10" },
    { name: "" },
    { is_active: "true" },
    { "number[gte]": "" },
    { place_type: "" },
    { site: "" },
  ];
  filterId = false;
  filterNom = false;
  filterNumber=false;
  filterPlaceType=false;
  filterSite=false;
  filterActif=false;
  newtPlace = {
    number: null,
    name: null,
    isActive: true,
    site: "",
    placeType: "",
  };
  updatePlace = {
    id: null
  };
  successAddSiteType = false;
  errorAddSiteType = false;
  successModifySiteType = false;
  failModifySiteType = false;
  places = [];
  sites=[]
  placeTypes=[];
  statut = {
    isActive: null,
  };
  loading = true;
  pagination = {
    first: 1,
    prev: 1,
    current: 1,
    next: 1,
    last: 1,
  };
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.user.isAdmin =
      this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    if (this.user.isAdmin) {
      this.filters[4].is_active = "";
    }
    this.getPlaces(1);
    this.countSiteType()
  }
  countSiteType(){
    this.share
      .getEntity(this.url, [{ page: 1 }, { itemsPerPage: "1" }])
      .subscribe((res) => {
        const total = res["hydra:totalItems"];
        this.totalCount = total;
      });
  }
  changeStatut(id, active: boolean) {
    this.statut.isActive = active ? false : true;
    this.share.updateEntity(this.url, id, this.statut).subscribe(
      (res) => {
        if (
          this.places.length == 1 &&
          this.pagination.current > 1 &&
          this.filters[4].is_active != ""
        ) {
          this.getPlaces(this.pagination.prev);
        } else {
          this.getPlaces(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
      }
    );
  }
  totalCount: number;
  getPlaces(page: number) {
    this.dataNotFound = false;
    this.isLoading = true;
    this.filters[1].page = page;
    this.share.getEntity(this.url, this.filters).subscribe(
      (res) => {
        this.isLoading = false;
        this.places = res["hydra:member"];
        console.log(res)
        if (this.places.length == 0) {
          this.dataNotFound = true;
        } else {
          this.dataNotFound = false;
        }
        if (page == 1) {
          this.pagination.prev = 1;
        } else {
          this.pagination.prev = parseInt(
            res["hydra:view"]["hydra:previous"]?.split("=").reverse()[0]
          );
        }
        if (
          page ==
          parseInt(res["hydra:view"]["hydra:last"]?.split("=").reverse()[0])
        ) {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
          );
        } else {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:next"]?.split("=").reverse()[0]
          );
        }
        this.pagination.current = page;
        this.pagination.first = parseInt(
          res["hydra:view"]["hydra:first"]?.split("=").reverse()[0]
        );
        this.pagination.last = parseInt(
          res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
        );
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
        this.isLoading = false;
      }
    );
  }
  addNewPlace() {
    if (this.newtPlace.name==null){
      this.entityEmpty=true
    }else {
      if (!this.entityExist && !this.entityEmpty) {
        this.share.addEntity(this.url, this.newtPlace).subscribe(
          (res) => {
            this.resetAddModal()
            this.getPlaces(this.pagination.current);
            this.countSiteType()
            this.successAddSiteType = true;
          },
          (error) => {
            if (error.status == 401) {
              localStorage.clear()
              this.router.navigate(["/login"]);
            }
            console.log(error)
            this.successAddSiteType = false;
            this.errorAddSiteType = true;
          }
        );
      }
    }
  }
  resetAddModal() {
    if (this.addForm){
      Object.keys(this.addForm.controls).forEach((key) => {
        const control = this.addForm.controls[key];
        control.markAsPristine();
        control.markAsUntouched();
      });
    }
    this.newtPlace = {
      number: null,
      name: null,
      isActive: true,
      site: "",
      placeType: "",
    };
    this.successAddSiteType = false;
    this.errorAddSiteType = false;
    this.inputStyle.backgroundColor = "";
    this.entityExist = false;
    this.entityEmpty = false;

  }
  showUpdateModal(place) {
    this.showAddModal()
    this.updatePlace.id=place.id
    this.newtPlace = {
      number: place.number,
      name: place.name,
      isActive: place.is_active,
      site: place.site['@id'],
      placeType: place.place_type['@id']
    };
  }
  putSiteType() {
    this.share
      .updateEntity(this.url, this.updatePlace.id, this.newtPlace)
      .subscribe(
        (res) => {
          this.getPlaces(this.pagination.current);
          this.successModifySiteType = true;
          this.delay(5000).then((any) => {
            this.successModifySiteType = false;
          });
        },
        (error) => {
          if (error.status == 401) {
            localStorage.clear()
            this.router.navigate(["/login"]);
          } else if (error.status == 422) {
            this.failModifySiteType = true;
            this.delay(10000).then((any) => {
              this.failModifySiteType = false;
            });
          }
        }
      );
  }
  uniquenessCheck() {
    if (this.newtPlace.name){
      console.log("naaa",this.newtPlace.name)
      this.share
        .getEntity(this.url, [{name: this.newtPlace.name}])
        .subscribe((res) => {
          if (!this.newtPlace.name) {
            this.inputStyle.backgroundColor = "#fee2e1";
            this.entityEmpty = true;
          } else {
            let exist = false;
            if (res["hydra:member"].length > 0) {
              res["hydra:member"].map((item) => {
                if (item.name === this.newtPlace.name) {
                  exist = true;
                }
              });
              if (exist) {
                this.inputStyle.backgroundColor = "#fee2e1";
                this.entityExist = true;
                this.entityEmpty = false;
              } else {
                this.inputStyle.backgroundColor = "#dbf2e3";
                this.entityExist = false;
                this.entityEmpty = false;
              }
            } else {
              this.inputStyle.backgroundColor = "#dbf2e3";
              this.entityExist = false;
              this.entityEmpty = false;
            }
          }
        });
    }else {
      this.inputStyle.backgroundColor = "";
      this.entityEmpty = true;
    }
  }

  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("")
    );
  }
  showAddModal(){
    this.getSites()
    this.getPlaceType()
  }
  getSites() {
    this.share
      .getEntity("/sites", [{ is_active: "true" }])
      .subscribe((result) => {
        console.log(result)
        this.sites = result["hydra:member"];
      });
  }
  getPlaceType() {
    this.share
      .getEntity("/place_types", [{ is_active: "true" }])
      .subscribe((result) => {
        this.placeTypes = result["hydra:member"];
      });
  }
}
