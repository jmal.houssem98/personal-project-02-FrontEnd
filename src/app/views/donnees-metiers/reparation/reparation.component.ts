import { Component, OnInit, ViewChild } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { Router } from "@angular/router";
import { LoaderService } from "../../../shared/services/loader.service";
import { ModalDirective } from "ngx-bootstrap/modal";
@Component({
  selector: "app-reparation",
  templateUrl: "./reparation.component.html",
  styleUrls: ["./reparation.component.scss", "../donnees-metiers.module.scss"],
})
export class ReparationComponent implements OnInit {
  url = "/reparations";
  reparationTypeSelected = null;
  inputStyle = {
    backgroundColor: "",
  };
  inputStyle2 = {
    backgroundColor: "",
  };
  inputStyle3 = {
    backgroundColor: "",
  };
  isLoading = false;
  dataNotFound = false;
  emptyPick = false;

  entityExist = false;
  entityEmpty = false;
  user = {
    isAdmin: true,
  };
  filters = [
    { id: null },
    { page: 1 },
    { itemsPerPage: "10" },
    { service_provider: "" },
    { comment: "" },
    { sinister: "" },
    { "amount%5Bgte%5D": "" },
    { reparation_type: "" },
    { element: "" },
  ];
  filterId = false;
  filterFournisseur = false;
  filterComment = false;
  filterSinistre = false;
  filterAmount = false;
  filterActif = false;
  filterReparationType = false;
  filterElement = false;

  newtSiteType = {
    id: null,
    serviceProvider: null,
    reparationSendingDate: null,
    reparationEndDate: null,
    comment: null,
    sinister: true,
    amount: null,
    reparationType: "",
    element: "",
  };
  updateSiteType = {
    id: null,
    serviceProvider: null,
    reparationSendingDate: null,
    reparationEndDate: null,
    comment: null,
    sinister: null,
    amount: null,
    reparationType: "",
    element: "",
  };
  successAddSiteType = false;
  errorAddSiteType = false;
  successModifySiteType = false;
  failModifySiteType = false;
  siteTypes = [];
  reparationTypes = [];
  elements = [];

  statut = {
    sinister: null,
  };
  loading = true;
  pagination = {
    first: 1,
    prev: 1,
    current: 1,
    next: 1,
    last: 1,
  };
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.user.isAdmin =
      this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    this.getSiteTypes(1);
    this.share
      .getEntity(this.url, [{ page: 1 }, { itemsPerPage: "1" }])
      .subscribe((res) => {
        const total = res["hydra:totalItems"];
        this.totalCount = total;
      });
    this.getElements();
    this.getReparationTypes();
  }
  changeStatut(id, active: boolean) {
    this.statut.sinister = active ? false : true;
    this.share.updateEntity(this.url, id, this.statut).subscribe(
      (res) => {
        if (this.siteTypes.length == 1 && this.pagination.current > 1) {
          this.getSiteTypes(this.pagination.prev);
        } else {
          this.getSiteTypes(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
      }
    );
  }
  totalCount: number;
  getSiteTypes(page: number) {
    this.dataNotFound = false;
    this.isLoading = true;
    this.filters[1].page = page;
    this.share.getEntity(this.url, this.filters).subscribe(
      (res) => {
        this.isLoading = false;
        this.siteTypes = res["hydra:member"];
        if (this.siteTypes.length == 0) {
          this.dataNotFound = true;
        } else {
          this.dataNotFound = false;
        }
        if (page == 1) {
          this.pagination.prev = 1;
        } else {
          this.pagination.prev = parseInt(
            res["hydra:view"]["hydra:previous"]?.split("=").reverse()[0]
          );
        }
        if (
          page ==
          parseInt(res["hydra:view"]["hydra:last"]?.split("=").reverse()[0])
        ) {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
          );
        } else {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:next"]?.split("=").reverse()[0]
          );
        }
        this.pagination.current = page;
        this.pagination.first = parseInt(
          res["hydra:view"]["hydra:first"]?.split("=").reverse()[0]
        );
        this.pagination.last = parseInt(
          res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
        );
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
        this.isLoading = false;
      }
    );
  }
  addNewSiteType() {
    if (this.newtSiteType.serviceProvider == "") {
      this.emptyPick = true;
      return 0;
    } else {
      this.emptyPick = false;
    }
    this.share.addEntity(this.url, this.newtSiteType).subscribe(
      (res) => {
        this.emptyPick = false;
        this.getSiteTypes(this.pagination.current);
        this.newtSiteType = {
          id: null,
          serviceProvider: null,
          reparationSendingDate: null,
          reparationEndDate: null,
          comment: null,
          sinister: true,
          amount: null,
          reparationType: "",
          element: "",
        };
        this.errorAddSiteType = false;
        this.successAddSiteType = true;
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
        this.successAddSiteType = false;
        this.errorAddSiteType = true;
      }
    );
  }
  resetAddModal() {
    this.emptyPickElement = false;
    this.emptyPickReparationType = false;
    this.emptyDateReparationSendingDate = false;
    this.emptyDateReparationEndDate = false;
    this.isEmpty = false;
    this.isEmpty2 = false;
    this.isEmpty3 = false;

    this.newtSiteType = {
      id: null,
      serviceProvider: null,
      reparationSendingDate: null,
      reparationEndDate: null,
      comment: null,
      sinister: true,
      amount: null,
      reparationType: "",
      element: "",
    };
    this.successAddSiteType = false;
    this.errorAddSiteType = false;
    this.inputStyle.backgroundColor = "";
    this.inputStyle2.backgroundColor = "";
    this.inputStyle3.backgroundColor = "";
    this.entityExist = false;
    this.entityEmpty = false;
  }
  showUpdateModal(siteType) {
    this.getReparationTypes();
    this.updateSiteType = {
      id: siteType.id,
      serviceProvider: siteType.service_provider,
      reparationSendingDate: siteType.reparation_sending_date.split("T")[0],
      reparationEndDate: siteType.reparation_end_date.split("T")[0],
      comment: siteType.comment,
      sinister: siteType.sinister,
      amount: siteType.amount,
      reparationType: siteType.reparation_type
        ? siteType.reparation_type["@id"]
        : "",
      element: siteType.element ? siteType.element["@id"] : "",
    };
  }
  putSiteType() {
    this.share
      .updateEntity(this.url, this.updateSiteType.id, this.updateSiteType)
      .subscribe(
        (res) => {
          this.getSiteTypes(this.pagination.current);
          this.successModifySiteType = true;
          this.delay(5000).then((any) => {
            this.successModifySiteType = false;
          });
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          } else {
            this.failModifySiteType = true;
            this.delay(10000).then((any) => {
              this.failModifySiteType = false;
            });
          }
        }
      );
  }
  uniquenessCheck() {
    this.share
      .getEntity(this.url, [{ id: this.newtSiteType.id }])
      .subscribe((res) => {
        if (!this.newtSiteType.id) {
          this.inputStyle.backgroundColor = "#fee2e1";
          this.entityEmpty = true;
        } else {
          let exist = false;
          if (res["hydra:member"].length > 0) {
            res["hydra:member"].map((item) => {
              if (item.name === this.newtSiteType.id) {
                exist = true;
              }
            });
            if (exist) {
              this.inputStyle.backgroundColor = "#fee2e1";
              this.entityExist = true;
              this.entityEmpty = false;
            } else {
              this.inputStyle.backgroundColor = "#dbf2e3";
              this.entityExist = false;
              this.entityEmpty = false;
            }
          } else {
            this.inputStyle.backgroundColor = "#dbf2e3";
            this.entityExist = false;
            this.entityEmpty = false;
          }
        }
      });
  }
  getReparationTypes() {
    this.share.getEntity("/elements", []).subscribe((result) => {
      this.elements = result["hydra:member"];
    });
  }

  getElements() {
    this.share
      .getEntity("/reparation_types", [{ is_active: "true" }])
      .subscribe((result) => {
        this.reparationTypes = result["hydra:member"];
      });
  }
  showAddModal() {
    this.getReparationTypes();
  }

  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("fired")
    );
  }
  emptyPickElement = false;
  emptyPickReparationType = false;
  emptyDateReparationSendingDate = false;
  emptyDateReparationEndDate = false;
  isEmpty = false;
  isEmpty2 = false;
  isEmpty3 = false;

  checkNoEmptyFields() {
    if (!this.newtSiteType.element) {
      this.emptyPickElement = true;
    } else {
      this.emptyPickElement = false;
    }

    if (!this.newtSiteType.reparationType) {
      this.emptyPickReparationType = true;
    } else {
      this.emptyPickReparationType = false;
    }
    if (!this.newtSiteType.serviceProvider) {
      this.inputStyle.backgroundColor = "#fee2e1";
      this.isEmpty = true;
    } else {
      this.inputStyle.backgroundColor = "#dbf2e3";
      this.isEmpty = false;
    }
    if (!this.newtSiteType.amount) {
      this.inputStyle2.backgroundColor = "#fee2e1";
      this.isEmpty2 = true;
    } else {
      this.inputStyle2.backgroundColor = "#dbf2e3";
      this.isEmpty2 = false;
    }
    if (!this.newtSiteType.comment) {
      this.inputStyle3.backgroundColor = "#fee2e1";
      this.isEmpty3 = true;
    } else {
      this.inputStyle3.backgroundColor = "#dbf2e3";
      this.isEmpty3 = false;
    }

    if (!this.newtSiteType.reparationSendingDate) {
      this.emptyDateReparationSendingDate = true;
    } else {
      this.emptyDateReparationSendingDate = false;
    }

    if (!this.newtSiteType.reparationEndDate) {
      this.emptyDateReparationEndDate = true;
    } else {
      this.emptyDateReparationEndDate = false;
    }
  }

  CheckEmpty() {
    if (!this.newtSiteType.serviceProvider) {
      this.inputStyle.backgroundColor = "#fee2e1";
      this.isEmpty = true;
    } else {
      this.inputStyle.backgroundColor = "#dbf2e3";
      this.isEmpty = false;
    }
  }
  CheckEmpty2() {
    if (!this.newtSiteType.amount) {
      this.inputStyle2.backgroundColor = "#fee2e1";
      this.isEmpty2 = true;
    } else {
      this.inputStyle2.backgroundColor = "#dbf2e3";
      this.isEmpty2 = false;
    }
  }
  CheckEmpty3() {
    if (!this.newtSiteType.comment) {
      this.inputStyle3.backgroundColor = "#fee2e1";
      this.isEmpty3 = true;
    } else {
      this.inputStyle3.backgroundColor = "#dbf2e3";
      this.isEmpty3 = false;
    }
  }

  CheckEmptyEdit() {
    if (!this.updateSiteType.serviceProvider) {
      this.inputStyle.backgroundColor = "#fee2e1";
    } else {
      this.inputStyle.backgroundColor = "#dbf2e3";
    }
  }

  CheckEmpty2Edit() {
    if (!this.updateSiteType.amount) {
      this.inputStyle2.backgroundColor = "#fee2e1";
    } else {
      this.inputStyle2.backgroundColor = "#dbf2e3";
    }
  }
  CheckEmpty3Edit() {
    if (!this.updateSiteType.comment) {
      this.inputStyle3.backgroundColor = "#fee2e1";
    } else {
      this.inputStyle3.backgroundColor = "#dbf2e3";
    }
  }
}
