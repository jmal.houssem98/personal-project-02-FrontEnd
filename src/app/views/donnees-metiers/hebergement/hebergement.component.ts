import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {SharedService} from "../../../shared/services/shared.service";
import {Router} from "@angular/router";
import {LoaderService} from "../../../shared/services/loader.service";

@Component({
  selector: 'app-hebergement',
  templateUrl: './hebergement.component.html',
  styleUrls: ['../donnees-metiers.module.scss','./hebergement.component.scss']
})
export class HebergementComponent implements OnInit {
  url = "/accommodations";
  inputStyle = {
    backgroundColor: "",
  };
  addForm:NgForm
  isLoading = false;
  dataNotFound = false;
  entityExist = false;
  entityEmpty = false;
  user = {
    isAdmin: true,
  };
  filters = [
    { id: null },
    { page: 1 },
    { itemsPerPage: "10" },
    { "value[gte]": "" },
    { is_active: "true" },
    { leasing: "" },
    { "annual_rent[gte]": "" },
    { "commissioning_date[after]": "2022-01-01" },
    { state: "" },
    { place: null },
    { version: null },
    { installation_type: "" },
    { owner_type: "" },
    //{ "installation_date[after]": "" },
    //{ "removing_date[after]": "" },
  ];
  filterId = false;
  filterNom = false;
  filterPays=false;
  filterRef=false;
  filterNomUtilisateur=false;
  filterSiteType=false;
  filterSiteGroup=false;
  filterGroupingType=false;
  filterInstallationType=false;
  filterOwnerType=false;
  filterInstallationDate=false;
  filterRemovingDate=false;
  filterActif = false;
  newtSiteType = {
    value: null,
    leasing: null,
    annualRent:null,
    commissioningDate: null,
    state: "",
    place:"",
    version: "",
    installationType: "",
    installationDate: null,
    removingDate: null,
    ownerType: ""
  };
  updateSiteType = {
    id: null
  };
  successAddSiteType = false;
  errorAddSiteType = false;
  successModifySiteType = false;
  failModifySiteType = false;
  sites = [];
  states=[];
  versions=[];
  ownerTypes=[]
  places=[];
  installationTypes=[]
  statut = {
    isActive: null,
  };
  loading = true;
  pagination = {
    first: 1,
    prev: 1,
    current: 1,
    next: 1,
    last: 1,
  };
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.user.isAdmin =
      this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    if (this.user.isAdmin) {
      this.filters[4].is_active = "";
    }
    this.getSites(1);
    this.countSiteType()
  }
  countSiteType(){
    this.share
      .getEntity(this.url, [{ page: 1 }, { itemsPerPage: "1" }])
      .subscribe((res) => {
        const total = res["hydra:totalItems"];
        this.totalCount = total;
      });
  }
  changeStatut(id, active: boolean) {
    this.statut.isActive = active ? false : true;
    this.share.updateEntity(this.url, id, this.statut).subscribe(
      (res) => {
        if (
          this.sites.length == 1 &&
          this.pagination.current > 1 &&
          this.filters[4].is_active != ""
        ) {
          this.getSites(this.pagination.prev);
        } else {
          this.getSites(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
      }
    );
  }
  totalCount: number;
  getSites(page: number) {
    this.dataNotFound = false;
    this.isLoading = true;
    this.filters[1].page = page;
    this.share.getEntity(this.url, this.filters).subscribe(
      (res) => {
        this.isLoading = false;
        this.sites = res["hydra:member"];
        console.log(res)
        if (this.sites.length == 0) {
          this.dataNotFound = true;
        } else {
          this.dataNotFound = false;
        }
        if (page == 1) {
          this.pagination.prev = 1;
        } else {
          this.pagination.prev = parseInt(
            res["hydra:view"]["hydra:previous"]?.split("=").reverse()[0]
          );
        }
        if (
          page ==
          parseInt(res["hydra:view"]["hydra:last"]?.split("=").reverse()[0])
        ) {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
          );
        } else {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:next"]?.split("=").reverse()[0]
          );
        }
        this.pagination.current = page;
        this.pagination.first = parseInt(
          res["hydra:view"]["hydra:first"]?.split("=").reverse()[0]
        );
        this.pagination.last = parseInt(
          res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
        );
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
        this.isLoading = false;
      }
    );
  }
  addNewSiteType() {
    // if (this.newtSiteType.name==null){
    //   this.entityEmpty=true
    // }else {
    //   if (!this.entityExist && !this.entityEmpty) {
    console.log(this.newtSiteType)
        this.share.addEntity(this.url, this.newtSiteType).subscribe(
          (res) => {
            this.resetAddModal()
            this.getSites(this.pagination.current);
            this.countSiteType()
            this.successAddSiteType = true;
          },
          (error) => {
            if (error.status == 401) {
              localStorage.clear()
              this.router.navigate(["/login"]);
            }
            console.log(error)
            this.successAddSiteType = false;
            this.errorAddSiteType = true;
          }
        );
  }
  resetAddModal() {
    if (this.addForm){
      Object.keys(this.addForm.controls).forEach((key) => {
        const control = this.addForm.controls[key];
        control.markAsPristine();
        control.markAsUntouched();
      });
    }
    this.newtSiteType = {
      value: null,
      leasing: null,
      annualRent:null,
      commissioningDate: null,
      state: "",
      place:"",
      version: "",
      installationType: "",
      ownerType: "",
      installationDate: null,
      removingDate: null,
    };
    this.successAddSiteType = false;
    this.errorAddSiteType = false;
    this.inputStyle.backgroundColor = "";
    this.entityExist = false;
    this.entityEmpty = false;

  }
  showUpdateModal(site) {
    this.showAddModal()
    this.updateSiteType.id=site.id
    this.newtSiteType = {
      value: site.value,
      leasing: site.leasing,
      annualRent:site.annual_rent,
      commissioningDate: site.commissioning_date.split("T")[0],
      state: site.state['@id'],
      place:site.place['@id'],
      version: site.version['@id'],
      installationType: site.installation_type['@id'],
      ownerType: site.owner_type['@id'],
      installationDate: site.installation_date.split("T")[0],
      removingDate: site.removing_date.split("T")[0],
    };
  }
  putSiteType() {
    this.share
      .updateEntity(this.url, this.updateSiteType.id, this.newtSiteType)
      .subscribe(
        (res) => {
          this.getSites(this.pagination.current);
          this.successModifySiteType = true;
          this.delay(5000).then((any) => {
            this.successModifySiteType = false;
          });
        },
        (error) => {
          if (error.status == 401) {
            localStorage.clear()
            this.router.navigate(["/login"]);
          } else if (error.status == 422) {
            this.failModifySiteType = true;
            this.delay(10000).then((any) => {
              this.failModifySiteType = false;
            });
          }
        }
      );
  }
  // uniquenessCheck() {
  //   if (this.newtSiteType.name){
  //     console.log("naaa",this.newtSiteType.name)
  //     this.share
  //       .getEntity(this.url, [{name: this.newtSiteType.name}])
  //       .subscribe((res) => {
  //         if (!this.newtSiteType.name) {
  //           this.inputStyle.backgroundColor = "#fee2e1";
  //           this.entityEmpty = true;
  //         } else {
  //           let exist = false;
  //           if (res["hydra:member"].length > 0) {
  //             res["hydra:member"].map((item) => {
  //               if (item.name === this.newtSiteType.name) {
  //                 exist = true;
  //               }
  //             });
  //             if (exist) {
  //               this.inputStyle.backgroundColor = "#fee2e1";
  //               this.entityExist = true;
  //               this.entityEmpty = false;
  //             } else {
  //               this.inputStyle.backgroundColor = "#dbf2e3";
  //               this.entityExist = false;
  //               this.entityEmpty = false;
  //             }
  //           } else {
  //             this.inputStyle.backgroundColor = "#dbf2e3";
  //             this.entityExist = false;
  //             this.entityEmpty = false;
  //           }
  //         }
  //       });
  //   }else {
  //     this.inputStyle.backgroundColor = "";
  //     this.entityEmpty = true;
  //   }
  // }

  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("")
    );
  }
  showAddModal(){
    this.getStates()
    this.getPlaces()
    this.getVersions()
    this.getInstallationTypes()
    this.getOwnerTypes()
  }
  getStates() {
    this.share
      .getEntity("/accommodation_states", [{ is_active: "true" }])
      .subscribe((result) => {
        this.states = result["hydra:member"];
      });
  }
  getPlaces() {
    this.share
      .getEntity("/places", [{ is_active: "true" }])
      .subscribe((result) => {
        this.places = result["hydra:member"];
      });
  }
  getVersions() {
    this.share
      .getEntity("/versions", [{ is_active: "true" }])
      .subscribe((result) => {
        this.versions = result["hydra:member"];
      });
  }
  getInstallationTypes() {
    this.share
      .getEntity("/installation_types", [{ is_active: "true" }])
      .subscribe((result) => {
        this.installationTypes = result["hydra:member"];
      });
  }
  getOwnerTypes() {
    this.share
      .getEntity("/owner_types", [{ is_active: "true" }])
      .subscribe((result) => {
        this.ownerTypes = result["hydra:member"];
      });
  }
}
