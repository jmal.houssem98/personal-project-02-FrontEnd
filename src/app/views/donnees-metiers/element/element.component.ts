import { Component, OnInit } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { Router } from "@angular/router";
import { LoaderService } from "../../../shared/services/loader.service";
import { ModalDirective } from "ngx-bootstrap/modal";
@Component({
  selector: "app-element",
  templateUrl: "./element.component.html",
  styleUrls: ["./element.component.scss", "../donnees-metiers.module.scss"],
})
export class ElementComponent implements OnInit {
  url = "/elements";
  rentalTypeSelected = null;
  inputStyle = {
    backgroundColor: "",
  };
  isLoading = false;
  dataNotFound = false;
  emptyPick = false;
  entityExist = false;
  entityEmpty = false;
  user = {
    isAdmin: true,
  };

  filters = [
    { id: null },
    { page: 1 },
    { itemsPerPage: "10" },
    { accommodation: "" },
    { sinister: "" },
    { is_guaranteed: "" },
    { element_type: "" },
    { canvas_provider: "" },
    { manufacturer: "" },
    { state: "" },
    { accommodation: "" },
  ];
  filterId = false;
  filterNom = false;
  filterSinister = false;
  filterIsGuaranteed = false;
  filterElementType = false;
  filterCanvasProvider = false;
  filterManufacturer = false;
  filterState = false;
  filterAccommodation = false;

  newtSiteType = {
    recognitionCode: null,
    sinister: true,
    isGuaranteed: true,
    elementType: "",
    manufacturer: "",
    accommodation: "",
    state: "",
    installationDate: null,
    scrappingDate: null,
    guaranteeEndDate: null,
    canvasProvider: "",
    createdAt: "2022-03-01",
    updatedAt: "2022-03-01",
  };
  updateSiteType = {
    id: null,
    recognitionCode: null,
    sinister: true,
    isGuaranteed: true,
    elementType: "",
    manufacturer: "",
    accommodation: "",
    state: "",
    canvasProvider: "",
    installationDate: null,
    scrappingDate: null,
    guaranteeEndDate: null,
  };
  successAddSiteType = false;
  errorAddSiteType = false;
  successModifySiteType = false;
  failModifySiteType = false;
  siteTypes = [];
  elementTypes = [];

  statut = {
    sinister: null,
  };
  stat = {
    is_guaranteed: null,
  };
  loading = true;
  pagination = {
    first: 1,
    prev: 1,
    current: 1,
    next: 1,
    last: 1,
  };
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.user.isAdmin =
      this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    this.getSiteTypes(1);
    this.share
      .getEntity(this.url, [{ page: 1 }, { itemsPerPage: "1" }])
      .subscribe((res) => {
        const total = res["hydra:totalItems"];
        this.totalCount = total;
      });
    this.getElementType();
    this.getAccomodation();
    this.getCanvasProvider();
    this.getManufacturer();
    this.getState();
  }
  changeStatut(id, active: boolean) {
    this.statut.sinister = active ? false : true;
    this.share.updateEntity(this.url, id, this.statut).subscribe(
      (res) => {
        if (
          this.siteTypes.length == 1 &&
          this.pagination.current > 1 &&
          this.filters[4].sinister != ""
        ) {
          this.getSiteTypes(this.pagination.prev);
        } else {
          this.getSiteTypes(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
      }
    );
  }
  changeStatut2(id, active: boolean) {
    let isGua = active ? false : true;
    this.share.updateEntity(this.url, id, { isGuaranteed: isGua }).subscribe(
      (res) => {
        if (
          this.siteTypes.length == 1 &&
          this.pagination.current > 1 &&
          this.filters[4].sinister != "" &&
          this.filters[5].is_guaranteed != ""
        ) {
          this.getSiteTypes(this.pagination.prev);
        } else {
          this.getSiteTypes(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
      }
    );
  }
  totalCount: number;
  getSiteTypes(page: number) {
    this.dataNotFound = false;
    this.isLoading = true;
    this.filters[1].page = page;
    this.share.getEntity(this.url, this.filters).subscribe(
      (res) => {
        this.isLoading = false;
        this.siteTypes = res["hydra:member"];
        if (this.siteTypes.length == 0) {
          this.dataNotFound = true;
        } else {
          this.dataNotFound = false;
        }
        if (page == 1) {
          this.pagination.prev = 1;
        } else {
          this.pagination.prev = parseInt(
            res["hydra:view"]["hydra:previous"]?.split("=").reverse()[0]
          );
        }
        if (
          page ==
          parseInt(res["hydra:view"]["hydra:last"]?.split("=").reverse()[0])
        ) {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
          );
        } else {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:next"]?.split("=").reverse()[0]
          );
        }
        this.pagination.current = page;
        this.pagination.first = parseInt(
          res["hydra:view"]["hydra:first"]?.split("=").reverse()[0]
        );
        this.pagination.last = parseInt(
          res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
        );
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
        this.isLoading = false;
      }
    );
  }
  addNewSiteType() {
    if (this.newtSiteType.recognitionCode == "") {
      this.emptyPick = true;
      return 0;
    }
    if (!this.entityExist && !this.entityEmpty) {
      this.share.addEntity(this.url, this.newtSiteType).subscribe(
        (res) => {
          this.emptyPick = false;
          this.getSiteTypes(this.pagination.current);
          this.newtSiteType = {
            recognitionCode: null,
            sinister: true,
            isGuaranteed: true,
            elementType: null,
            manufacturer: null,
            accommodation: null,
            state: null,
            canvasProvider: null,
            installationDate: null,
            scrappingDate: null,
            guaranteeEndDate: null,
            createdAt: "2022-03-22",
            updatedAt: "2022-03-01",
          };
          this.errorAddSiteType = false;
          this.successAddSiteType = true;
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          }
          this.successAddSiteType = false;
          this.errorAddSiteType = true;
        }
      );
    }
  }
  resetAddModal() {
    this.emptyPickElement = false;
    this.emptyPickCanvasProvider = false;
    this.emptyPickManufacturer = false;
    this.emptyPickAccommodation = false;
    this.emptyPickState = false;

    this.emptyDateInstallationDate = false;
    this.emptyDateScrappingDate = false;
    this.emptyDateGuaranteeEndDate = false;

    this.newtSiteType = {
      recognitionCode: null,
      sinister: true,
      isGuaranteed: true,
      elementType: "",
      manufacturer: "",
      accommodation: "",
      state: "",
      canvasProvider: "",
      installationDate: null,
      scrappingDate: null,
      guaranteeEndDate: null,
      createdAt: "2022-03-22",
      updatedAt: "2022-03-01",
    };
    this.successAddSiteType = false;
    this.errorAddSiteType = false;
    this.inputStyle.backgroundColor = "";
    this.entityExist = false;
    this.entityEmpty = false;
  }
  showUpdateModal(siteType) {
    this.getElementType();
    this.getAccomodation();
    this.getCanvasProvider();
    this.getManufacturer();
    this.getState();
    this.updateSiteType = {
      id: siteType.id,
      recognitionCode: siteType.recognition_code,
      sinister: siteType.sinister,
      isGuaranteed: siteType.isGuaranteed,
      elementType: siteType.element_type ? siteType.element_type["@id"] : "",
      manufacturer: siteType.manufacturer ? siteType.manufacturer["@id"] : "",
      accommodation: siteType.accommodation
        ? siteType.accommodation["@id"]
        : "",
      state: siteType.state ? siteType.state["@id"] : "",
      canvasProvider: siteType.canvas_provider
        ? siteType.canvas_provider["@id"]
        : "",
      installationDate: siteType.installation_date.split("T")[0],
      scrappingDate: siteType.scrapping_date.split("T")[0],
      guaranteeEndDate: siteType.guarantee_end_date.split("T")[0],
    };
  }

  putSiteType() {
    this.share
      .updateEntity(this.url, this.updateSiteType.id, this.updateSiteType)
      .subscribe(
        (res) => {
          this.getSiteTypes(this.pagination.current);
          this.successModifySiteType = true;
          this.delay(5000).then((any) => {
            this.successModifySiteType = false;
          });
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          } else if (error.status == 422) {
            this.failModifySiteType = true;
            this.delay(10000).then((any) => {
              this.failModifySiteType = false;
            });
          }
        }
      );
  }
  uniquenessCheck() {
    this.share
      .getEntity(this.url, [
        { recognition_code: this.newtSiteType.recognitionCode },
      ])
      .subscribe((res) => {
        if (!this.newtSiteType.recognitionCode) {
          console.log("items already exist !!!");

          this.inputStyle.backgroundColor = "#fee2e1";
          this.entityEmpty = true;
        } else {
          let exist = false;
          if (res["hydra:member"].length > 0) {
            res["hydra:member"].map((item) => {
              if (item.recognition_code === this.newtSiteType.recognitionCode) {
                exist = true;
              }
            });
            if (exist) {
              this.inputStyle.backgroundColor = "#fee2e1";
              this.entityExist = true;
              this.entityEmpty = false;
              console.log("items already exist !!!");
            } else {
              this.inputStyle.backgroundColor = "#dbf2e3";
              this.entityExist = false;
              this.entityEmpty = false;
            }
          } else {
            this.inputStyle.backgroundColor = "#dbf2e3";
            this.entityExist = false;
            this.entityEmpty = false;
          }
        }
      });
  }

  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("fired")
    );
  }
  showAddModal() {
    this.getElementType();
    this.getAccomodation();
    this.getCanvasProvider();
    this.getManufacturer();
    this.getState();
  }

  accommodations = [];
  canvasProviders = [];
  manufacturers = [];
  states = [];

  getElementType() {
    this.share.getEntity("/element_types", []).subscribe((result) => {
      this.elementTypes = result["hydra:member"];
    });
  }

  getAccomodation() {
    this.share.getEntity("/accommodations", []).subscribe((result) => {
      this.accommodations = result["hydra:member"];
    });
  }
  getCanvasProvider() {
    this.share.getEntity("/canvas_providers", []).subscribe((result) => {
      this.canvasProviders = result["hydra:member"];
    });
  }
  getManufacturer() {
    this.share.getEntity("/manufacturers", []).subscribe((result) => {
      this.manufacturers = result["hydra:member"];
    });
  }
  getState() {
    this.share
      .getEntity("/element_states", [{ is_active: "true" }])
      .subscribe((result) => {
        this.states = result["hydra:member"];
      });
  }

  CheckUniqueForUpdate() {
    if (!this.updateSiteType.recognitionCode) {
      this.inputStyle.backgroundColor = "#fee2e1";
    } else {
      this.inputStyle.backgroundColor = "#dbf2e3";
    }
  }

  emptyPickElement = false;
  emptyPickCanvasProvider = false;
  emptyPickManufacturer = false;
  emptyPickAccommodation = false;
  emptyPickState = false;
  emptyDateInstallationDate = false;
  emptyDateScrappingDate = false;
  emptyDateGuaranteeEndDate = false;

  checkNoEmptyFields() {
    if (!this.newtSiteType.elementType) {
      this.emptyPickElement = true;
    } else {
      this.emptyPickElement = false;
    }

    if (!this.newtSiteType.canvasProvider) {
      this.emptyPickCanvasProvider = true;
    } else {
      this.emptyPickCanvasProvider = false;
    }

    if (!this.newtSiteType.manufacturer) {
      this.emptyPickManufacturer = true;
    } else {
      this.emptyPickManufacturer = false;
    }

    if (!this.newtSiteType.accommodation) {
      this.emptyPickAccommodation = true;
    } else {
      this.emptyPickAccommodation = false;
    }

    if (!this.newtSiteType.state) {
      this.emptyPickState = true;
    } else {
      this.emptyPickState = false;
    }

    if (!this.newtSiteType.installationDate) {
      this.emptyDateInstallationDate = true;
    } else {
      this.emptyDateInstallationDate = false;
    }

    if (!this.newtSiteType.scrappingDate) {
      this.emptyDateScrappingDate = true;
    } else {
      this.emptyDateScrappingDate = false;
    }

    if (!this.newtSiteType.guaranteeEndDate) {
      this.emptyDateGuaranteeEndDate = true;
    } else {
      this.emptyDateGuaranteeEndDate = false;
    }
  }
  elementHistory = [];
  loadingdElementHistory = false;
  getElementHistory(id: any) {
    this.loadingdElementHistory = true;
    this.elementHistory = [];
    this.share
      .getEntity("/element_histories", [{ element: id }])
      .subscribe((result) => {
        this.elementHistory = result["hydra:member"];
        this.loadingdElementHistory = false;
      });
  }
}
