import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeDElementComponent } from './type-d-element.component';

describe('TypeDElementComponent', () => {
  let component: TypeDElementComponent;
  let fixture: ComponentFixture<TypeDElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeDElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeDElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
