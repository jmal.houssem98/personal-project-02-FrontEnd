import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtatHebergementComponent } from './etat-hebergement.component';

describe('EtatHebergementComponent', () => {
  let component: EtatHebergementComponent;
  let fixture: ComponentFixture<EtatHebergementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtatHebergementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtatHebergementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
