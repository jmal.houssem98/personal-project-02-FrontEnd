import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeLocatifComponent } from './type-locatif.component';

describe('TypeLocatifComponent', () => {
  let component: TypeLocatifComponent;
  let fixture: ComponentFixture<TypeLocatifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeLocatifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeLocatifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
