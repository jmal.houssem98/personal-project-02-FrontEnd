import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfectionneursComponent } from './confectionneurs.component';

describe('ConfectionneursComponent', () => {
  let component: ConfectionneursComponent;
  let fixture: ComponentFixture<ConfectionneursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfectionneursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfectionneursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
