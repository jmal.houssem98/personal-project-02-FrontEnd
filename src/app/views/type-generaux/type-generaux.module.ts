import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TypeGenerauxRoutingModule } from "./type-generaux-routing.module";
import { FormsModule } from "@angular/forms";
import { ModalModule } from "ngx-bootstrap/modal";
import { TypeDeReparationComponent } from "./type-de-reparation/type-de-reparation.component";
import { TypeDeSiteComponent } from "./type-de-site/type-de-site.component";
import { TypeDInstallationComponent } from "./type-d-installation/type-d-installation.component";
import { TypeLieuComponent } from "./type-lieu/type-lieu.component";
import { TypeLocatifComponent } from "./type-locatif/type-locatif.component";
import { TypeHebergementComponent } from "./type-hebergement/type-hebergement.component";
import { ProprietaireComponent } from "./proprietaire/proprietaire.component";
import { EtatHebergementComponent } from "./etat-hebergement/etat-hebergement.component";
import { FournisseursToileComponent } from "./fournisseurs-toile/fournisseurs-toile.component";
import { RegroupementsComponent } from "./regroupements/regroupements.component";
import { EtatDElementsComponent } from "./etat-d-elements/etat-d-elements.component";
import { ConfectionneursComponent } from "./confectionneurs/confectionneurs.component";
import { GroupeDeSiteComponent } from "./groupe-de-site/groupe-de-site.component";
import { TypeDElementComponent } from "./type-d-element/type-d-element.component";

@NgModule({
  declarations: [
    TypeDeSiteComponent,
    TypeHebergementComponent,
    TypeLieuComponent,
    TypeLocatifComponent,
    TypeDeReparationComponent,
    TypeDInstallationComponent,
    ProprietaireComponent,
    EtatHebergementComponent,
    FournisseursToileComponent,
    TypeDeReparationComponent,
    TypeDInstallationComponent,
    RegroupementsComponent,
    EtatDElementsComponent,
    ConfectionneursComponent,
    GroupeDeSiteComponent,
    TypeDElementComponent,
  ],
  imports: [
    CommonModule,
    TypeGenerauxRoutingModule,
    FormsModule,
    ModalModule.forRoot(),
  ],
})
export class TypeGenerauxModule {}
