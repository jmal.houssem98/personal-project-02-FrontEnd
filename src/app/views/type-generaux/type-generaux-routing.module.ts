import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TypeDeSiteComponent } from "./type-de-site/type-de-site.component";
import { TypeDeReparationComponent } from "./type-de-reparation/type-de-reparation.component";
import { TypeDInstallationComponent } from "./type-d-installation/type-d-installation.component";
import { TypeLieuComponent } from "./type-lieu/type-lieu.component";
import { TypeLocatifComponent } from "./type-locatif/type-locatif.component";
import { TypeHebergementComponent } from "./type-hebergement/type-hebergement.component";
import { ProprietaireComponent } from "./proprietaire/proprietaire.component";
import { EtatHebergementComponent } from "./etat-hebergement/etat-hebergement.component";
import { FournisseursToileComponent } from "./fournisseurs-toile/fournisseurs-toile.component";

import { RegroupementsComponent } from "./regroupements/regroupements.component";
import { EtatDElementsComponent } from "./etat-d-elements/etat-d-elements.component";
import { ConfectionneursComponent } from "./confectionneurs/confectionneurs.component";
import { GroupeDeSiteComponent } from "./groupe-de-site/groupe-de-site.component";
import { TypeDElementComponent } from "./type-d-element/type-d-element.component";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Type généraux",
    },
    children: [
      {
        path: "",
        redirectTo: "type-de-site",
      },
      {
        path: "type-de-site",
        component: TypeDeSiteComponent,
        data: {
          title: "Type de site",
        },
      },
      {
        path: "type-d'hébergement",
        component: TypeHebergementComponent,
        data: {
          title: "Type d'hébergement",
        },
      },
      {
        path: "type-de-lieu",
        component: TypeLieuComponent,
        data: {
          title: "Type de lieu",
        },
      },
      {
        path: "type-de-locatif",
        component: TypeLocatifComponent,
        data: {
          title: "Type de locatif",
        },
      },
      {
        path: "type-de-reparation",
        component: TypeDeReparationComponent,
        data: {
          title: "Type de reparation",
        },
      },
      {
        path: "type-d-installation",
        component: TypeDInstallationComponent,
        data: {
          title: "Type de reparation",
        },
      },
      {
        path: "type-d-element",
        component: TypeDElementComponent,
        data: {
          title: "Type d'élément",
        },
      },
      {
        path: "propriétaire",
        component: ProprietaireComponent,
        data: {
          title: "propriétaire",
        },
      },
      {
        path: "état-d'hébergements",
        component: EtatHebergementComponent,
        data: {
          title: "état d'hébergements",
        },
      },
      {
        path: "fournisseurs-de-toiles",
        component: FournisseursToileComponent,
        data: {
          title: "Fournisseurs de toiles",
        },
      },
      {
        path: "regroupements",
        component: RegroupementsComponent,
        data: {
          title: "Regroupements",
        },
      },
      {
        path: "etat-d-elements",
        component: EtatDElementsComponent,
        data: {
          title: "État d'éléments",
        },
      },
      {
        path: "confectionneurs",
        component: ConfectionneursComponent,
        data: {
          title: "confectionneurs",
        },
      },
      {
        path: "groupe-de-site",
        component: GroupeDeSiteComponent,
        data: {
          title: "groupe de site",
        },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TypeGenerauxRoutingModule {}
