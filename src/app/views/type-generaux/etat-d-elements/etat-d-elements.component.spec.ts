import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtatDElementsComponent } from './etat-d-elements.component';

describe('EtatDElementsComponent', () => {
  let component: EtatDElementsComponent;
  let fixture: ComponentFixture<EtatDElementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtatDElementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtatDElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
