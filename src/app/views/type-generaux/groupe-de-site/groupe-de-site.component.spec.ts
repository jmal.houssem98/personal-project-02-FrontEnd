import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupeDeSiteComponent } from './groupe-de-site.component';

describe('GroupeDeSiteComponent', () => {
  let component: GroupeDeSiteComponent;
  let fixture: ComponentFixture<GroupeDeSiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupeDeSiteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupeDeSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
