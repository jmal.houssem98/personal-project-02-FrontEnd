import { Component, OnInit, ViewChild } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { Router } from "@angular/router";
import { LoaderService } from "../../../shared/services/loader.service";
import { ModalDirective } from "ngx-bootstrap/modal";
@Component({
  selector: "app-type-de-reparation",
  templateUrl: "./type-de-reparation.component.html",
  styleUrls: ["../type-generaux.module.scss","./type-de-reparation.component.scss"],
})
export class TypeDeReparationComponent implements OnInit {
  url = "/reparation_types";
  rentalTypeSelected = null;
  inputStyle = {
    backgroundColor: "",
  };
  isLoading = false;
  dataNotFound = false;
  emptyPick = false;

  entityExist = false;
  entityEmpty = false;
  user = {
    isAdmin: true,
  };
  filters = [
    { id: null },
    { page: 1 },
    { itemsPerPage: "10" },
    { name: "" },
    { is_active: "true" },
    { rental_type: "" },
  ];
  filterId = false;
  filterNom = false;
  filterActif = false;
  filterRentalType = false;
  newtSiteType = {
    isActive: true,
    name: null,
    rentalType: "",
  };
  updateSiteType = {
    id: null,
    isActive: true,
    name: null,
    rentalType: "",
  };
  successAddSiteType = false;
  errorAddSiteType = false;
  successModifySiteType = false;
  failModifySiteType = false;
  siteTypes = [];
  rentalTypes = [];

  statut = {
    isActive: null,
  };
  loading = true;
  pagination = {
    first: 1,
    prev: 1,
    current: 1,
    next: 1,
    last: 1,
  };
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.getActiveList();
    this.user.isAdmin =
      this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    if (this.user.isAdmin) {
      this.filters[4].is_active = "";
    }
    this.getSiteTypes(1);
    this.share
      .getEntity(this.url, [{ page: 1 }, { itemsPerPage: "1" }])
      .subscribe((res) => {
        const total = res["hydra:totalItems"];
        this.totalCount = total;
      });
  }
  changeStatut(id, active: boolean) {
    this.statut.isActive = active ? false : true;
    this.share.updateEntity(this.url, id, this.statut).subscribe(
      (res) => {
        if (
          this.siteTypes.length == 1 &&
          this.pagination.current > 1 &&
          this.filters[4].is_active != ""
        ) {
          this.getSiteTypes(this.pagination.prev);
        } else {
          this.getSiteTypes(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
      }
    );
  }
  totalCount: number;
  getSiteTypes(page: number) {
    this.dataNotFound = false;
    this.isLoading = true;
    this.filters[1].page = page;
    this.share.getEntity(this.url, this.filters).subscribe(
      (res) => {
        this.isLoading = false;
        this.siteTypes = res["hydra:member"];
        if (this.siteTypes.length == 0) {
          this.dataNotFound = true;
        } else {
          this.dataNotFound = false;
        }
        if (page == 1) {
          this.pagination.prev = 1;
        } else {
          this.pagination.prev = parseInt(
            res["hydra:view"]["hydra:previous"]?.split("=").reverse()[0]
          );
        }
        if (
          page ==
          parseInt(res["hydra:view"]["hydra:last"]?.split("=").reverse()[0])
        ) {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
          );
        } else {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:next"]?.split("=").reverse()[0]
          );
        }
        this.pagination.current = page;
        this.pagination.first = parseInt(
          res["hydra:view"]["hydra:first"]?.split("=").reverse()[0]
        );
        this.pagination.last = parseInt(
          res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
        );
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
        this.isLoading = false;
      }
    );
  }
  addNewSiteType() {
    if (this.newtSiteType.rentalType == "") {
      this.emptyPick = true;
      return 0;
    }
    if (!this.entityExist && !this.entityEmpty) {
      this.share.addEntity(this.url, this.newtSiteType).subscribe(
        (res) => {
          this.emptyPick = false;
          this.getSiteTypes(this.pagination.current);
          this.newtSiteType = {
            isActive: true,
            name: null,
            rentalType: null,
          };
          this.errorAddSiteType = false;
          this.successAddSiteType = true;
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          }
          this.successAddSiteType = false;
          this.errorAddSiteType = true;
        }
      );
    }
  }
  resetAddModal() {
    this.newtSiteType = {
      isActive: true,
      name: null,
      rentalType: null,
    };
    this.successAddSiteType = false;
    this.errorAddSiteType = false;
    this.inputStyle.backgroundColor = "";
    this.entityExist = false;
    this.entityEmpty = false;
  }
  showUpdateModal(siteType) {
    this.updateSiteType = {
      id: siteType.id,
      isActive: siteType.isActive,
      name: siteType.name,
      rentalType: siteType.rentalType,
    };
  }
  putSiteType() {
    this.share
      .updateEntity(this.url, this.updateSiteType.id, this.updateSiteType)
      .subscribe(
        (res) => {
          this.getSiteTypes(this.pagination.current);
          this.successModifySiteType = true;
          this.delay(5000).then((any) => {
            this.successModifySiteType = false;
          });
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          } else if (error.status == 422) {
            this.failModifySiteType = true;
            this.delay(10000).then((any) => {
              this.failModifySiteType = false;
            });
          }
        }
      );
  }
  uniquenessCheck() {
    this.share
      .getEntity(this.url, [{ name: this.newtSiteType.name }])
      .subscribe((res) => {
        if (!this.newtSiteType.name) {
          this.inputStyle.backgroundColor = "#fee2e1";
          this.entityEmpty = true;
        } else {
          let exist = false;
          if (res["hydra:member"].length > 0) {
            res["hydra:member"].map((item) => {
              if (item.name === this.newtSiteType.name) {
                exist = true;
              }
            });
            if (exist) {
              this.inputStyle.backgroundColor = "#fee2e1";
              this.entityExist = true;
              this.entityEmpty = false;
            } else {
              this.inputStyle.backgroundColor = "#dbf2e3";
              this.entityExist = false;
              this.entityEmpty = false;
            }
          } else {
            this.inputStyle.backgroundColor = "#dbf2e3";
            this.entityExist = false;
            this.entityEmpty = false;
          }
        }
      });
  }

  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("fired")
    );
  }

  getActiveList() {
    this.share.getEntity("/rental_types", this.filters).subscribe((res) => {
      this.rentalTypes = res["hydra:member"];
    });
  }
}
