import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeDeReparationComponent } from './type-de-reparation.component';

describe('TypeDeReparationComponent', () => {
  let component: TypeDeReparationComponent;
  let fixture: ComponentFixture<TypeDeReparationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeDeReparationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeDeReparationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
