import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeDInstallationComponent } from './type-d-installation.component';

describe('TypeDInstallationComponent', () => {
  let component: TypeDInstallationComponent;
  let fixture: ComponentFixture<TypeDInstallationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeDInstallationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeDInstallationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
