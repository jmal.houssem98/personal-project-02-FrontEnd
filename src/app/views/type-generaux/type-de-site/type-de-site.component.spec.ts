import { ComponentFixture, TestBed } from "@angular/core/testing";

import { TypeDeSiteComponent } from "./type-de-site.component";

describe("TypeDeSiteComponent", () => {
  let component: TypeDeSiteComponent;
  let fixture: ComponentFixture<TypeDeSiteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TypeDeSiteComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeDeSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
