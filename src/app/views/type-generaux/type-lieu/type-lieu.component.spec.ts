import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeLieuComponent } from './type-lieu.component';

describe('TypeLieuComponent', () => {
  let component: TypeLieuComponent;
  let fixture: ComponentFixture<TypeLieuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeLieuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeLieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
