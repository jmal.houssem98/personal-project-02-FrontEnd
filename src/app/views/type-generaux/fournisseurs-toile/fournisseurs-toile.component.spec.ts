import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FournisseursToileComponent } from './fournisseurs-toile.component';

describe('FournisseursToileComponent', () => {
  let component: FournisseursToileComponent;
  let fixture: ComponentFixture<FournisseursToileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FournisseursToileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FournisseursToileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
