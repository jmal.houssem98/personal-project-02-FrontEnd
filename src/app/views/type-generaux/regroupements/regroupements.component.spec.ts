import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegroupementsComponent } from './regroupements.component';

describe('RegroupementsComponent', () => {
  let component: RegroupementsComponent;
  let fixture: ComponentFixture<RegroupementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegroupementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegroupementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
