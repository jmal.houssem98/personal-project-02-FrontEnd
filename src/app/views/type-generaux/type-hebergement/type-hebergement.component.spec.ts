import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeHebergementComponent } from './type-hebergement.component';

describe('TypeHebergementComponent', () => {
  let component: TypeHebergementComponent;
  let fixture: ComponentFixture<TypeHebergementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeHebergementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeHebergementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
