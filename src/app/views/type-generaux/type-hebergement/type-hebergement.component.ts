import { Component, OnInit } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { Router } from "@angular/router";
import { LoaderService } from "../../../shared/services/loader.service";
@Component({
  selector: "app-type-hebergement",
  templateUrl: "./type-hebergement.component.html",
  styleUrls: [
    "../type-generaux.module.scss",
    "./type-hebergement.component.scss",
  ],
})
export class TypeHebergementComponent implements OnInit {
  inputStyle = {
    backgroundColor: "",
  };

  isLoading = false;
  dataNotFound = false;
  entityExist = false;
  entityEmpty = false;
  emptyPick = false;

  user = {
    isAdmin: true,
  };
  url = "/accommodation_types";
  filters = [
    { id: null },
    { page: 1 },
    { itemsPerPage: "10" },
    { name: "" },
    { is_active: "true" },
    { "amortization%5Bgte%5D": "" },
    { "guarantee%5Bgte%5D": "" },
    { rental_type: "" },
  ];
  filterId = false;
  filterNom = false;
  filterActif = false;
  filterGuarantee = false;
  filterAmortization = false;
  filterRentalType = false;
  newAccommodationType = {
    isActive: true,
    name: null,
    guarantee: 0,
    amortization: 0,
    rentalType: "",
  };
  updateAccommodationType = {
    id: null,
    isActive: true,
    name: null,
    guarantee: null,
    amortization: null,
    rentalType: null,
  };
  rentalTypes: [];
  successAddAccommodationType = false;
  errorAddAccommodationType = false;
  successModifySiteType = false;
  failModifySiteType = false;
  AccommodationTypes = [];
  statut = {
    isActive: null,
  };
  loading = true;
  pagination = {
    first: 1,
    prev: 1,
    current: 1,
    next: 1,
    last: 1,
  };
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.user.isAdmin =
      this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    if (this.user.isAdmin) {
      this.filters[4].is_active = "";
    }
    this.getAccommodationType(1);
    this.share
      .getEntity(this.url, [{ page: 1 }, { itemsPerPage: "1" }])
      .subscribe((res) => {
        const total = res["hydra:totalItems"];
        this.totalCount = total;
      });
  }
  changeStatut(id, active: boolean) {
    this.statut.isActive = active ? false : true;
    this.share.updateEntity(this.url, id, this.statut).subscribe(
      (res) => {
        if (
          this.AccommodationTypes.length == 1 &&
          this.pagination.current > 1 &&
          this.filters[4].is_active != ""
        ) {
          this.getAccommodationType(this.pagination.prev);
        } else {
          this.getAccommodationType(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
      }
    );
  }
  totalCount: number;
  getAccommodationType(page: number) {
    this.dataNotFound = false;
    this.isLoading = true;
    this.filters[1].page = page;
    this.share.getEntity(this.url, this.filters).subscribe(
      (res) => {
        this.isLoading = false;
        this.AccommodationTypes = res["hydra:member"];
        if (this.AccommodationTypes.length == 0) {
          this.dataNotFound = true;
        } else {
          this.dataNotFound = false;
        }
        if (page == 1) {
          this.pagination.prev = 1;
        } else {
          this.pagination.prev = parseInt(
            res["hydra:view"]["hydra:previous"]?.split("=").reverse()[0]
          );
        }
        if (
          page ==
          parseInt(res["hydra:view"]["hydra:last"]?.split("=").reverse()[0])
        ) {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
          );
        } else {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:next"]?.split("=").reverse()[0]
          );
        }
        this.pagination.current = page;
        this.pagination.first = parseInt(
          res["hydra:view"]["hydra:first"]?.split("=").reverse()[0]
        );
        this.pagination.last = parseInt(
          res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
        );
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
        this.isLoading = false;
      }
    );
  }
  addNewSiteType() {
    if (this.newAccommodationType.rentalType == "") {
      this.emptyPick = true;
      return 0;
    }
    if (!this.entityExist && !this.entityEmpty) {
      this.share.addEntity(this.url, this.newAccommodationType).subscribe(
        (res) => {
          this.emptyPick = false;

          this.getAccommodationType(this.pagination.current);
          this.newAccommodationType = {
            isActive: true,
            name: null,
            guarantee: null,
            amortization: null,
            rentalType: null,
          };
          this.errorAddAccommodationType = false;
          this.successAddAccommodationType = true;
          this.inputStyle.backgroundColor = "";
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          }
          this.successAddAccommodationType = false;
          this.errorAddAccommodationType = true;
        }
      );
    }
  }
  resetAddModal() {
    this.newAccommodationType = {
      isActive: true,
      name: null,
      guarantee: null,
      amortization: null,
      rentalType: null,
    };
    this.successAddAccommodationType = false;
    this.errorAddAccommodationType = false;
    this.inputStyle.backgroundColor = "";
    this.entityExist = false;
    this.entityEmpty = false;
  }
  showUpdateModal(accommodationType) {
    this.getRentalTypes();
    this.updateAccommodationType = {
      id: accommodationType.id,
      isActive: accommodationType.isActive,
      name: accommodationType.name,
      guarantee: accommodationType.guarantee,
      amortization: accommodationType.amortization,
      rentalType: accommodationType.rental_type
        ? accommodationType.rental_type["@id"]
        : "",
    };
  }
  putSiteType() {
    this.share
      .updateEntity(
        this.url,
        this.updateAccommodationType.id,
        this.updateAccommodationType
      )
      .subscribe(
        (res) => {
          this.getAccommodationType(this.pagination.current);
          this.successModifySiteType = true;
          this.delay(5000).then((any) => {
            this.successModifySiteType = false;
          });
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          } else if (error.status == 422) {
            this.failModifySiteType = true;
            this.delay(10000).then((any) => {
              this.failModifySiteType = false;
            });
          }
        }
      );
  }
  uniquenessCheck() {
    this.share
      .getEntity(this.url, [{ name: this.newAccommodationType.name }])
      .subscribe((res) => {
        console.log(res);
        if (!this.newAccommodationType.name) {
          this.inputStyle.backgroundColor = "#fee2e1";
          this.entityEmpty = true;
        } else {
          let exist = false;
          if (res["hydra:member"].length > 0) {
            res["hydra:member"].map((item) => {
              if (item.name === this.newAccommodationType.name) {
                exist = true;
              }
            });
            if (exist) {
              this.inputStyle.backgroundColor = "#fee2e1";
              this.entityExist = true;
              this.entityEmpty = false;
            } else {
              this.inputStyle.backgroundColor = "#dbf2e3";
              this.entityExist = false;
              this.entityEmpty = false;
            }
          } else {
            this.inputStyle.backgroundColor = "#dbf2e3";
            this.entityExist = false;
            this.entityEmpty = false;
          }
        }
      });
  }
  getRentalTypes() {
    this.share
      .getEntity("/rental_types", [{ is_active: "true" }])
      .subscribe((result) => {
        this.rentalTypes = result["hydra:member"];
        console.log("rental", this.rentalTypes);
      });
  }
  showAddModal() {
    this.getRentalTypes();
  }
  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("fired")
    );
  }
}
