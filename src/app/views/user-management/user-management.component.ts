import { Component, OnInit } from "@angular/core";
import { SharedService } from "../../shared/services/shared.service";
import { Router } from "@angular/router";
import { LoaderService } from "../../shared/services/loader.service";
import { log } from "util";

@Component({
  templateUrl: "./user-management.component.html",
  styleUrls: ["./user-management.component.scss"],
})
export class UserManagementComponent implements OnInit {
  url = "/users";
  inputStyle = {
    backgroundColor: "",
  };
  isLoading = false;
  isAdding = false;
  dataNotFound = false;
  entityExist = false;
  entityEmpty = false;
  user = {
    isAdmin: true,
  };
  syntaxMail = false;
  filters = [
    { id: null },
    { page: 1 },
    { itemsPerPage: "10" },
    { email: "" },
    { is_active: "true" },
    { last_name: "" },
    { first_name: "" },
  ];
  filterId = false;
  filterNom = false;
  filterRole = false;
  filterEmail = false;
  filterLastName = false;
  filterActif = false;
  newUser = {
    email: null,
    password: Math.random().toString(36).slice(-10),
    username: "Pseudo",
    roles: ["ROLE_USER"],
    lastName: null,
    firstName: null,
    isActive: true,
    confirmedAccount: false,
    language: "fr",
  };
  updateUser = {
    id: null,
    email: null,
    username: null,
    lastName: null,
    firstName: null,
    isActive: true,
    roles: [],
  };
  successAddUser = false;
  errorAddUser = false;
  successModifyUser = false;
  failModifyUser = false;
  users = [];
  statut = {
    isActive: null,
  };
  loading = true;
  pagination = {
    first: 1,
    prev: 1,
    current: 1,
    next: 1,
    last: 1,
  };
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.user.isAdmin =
      this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    if (this.user.isAdmin) {
      this.filters[4].is_active = "";
    } else {
      this.router.navigate(["/dashboard"]);
    }
    this.getUsers(1);
    this.countUsers();
  }
  countUsers() {
    this.share
      .getEntity(this.url, [{ page: 1 }, { itemsPerPage: "1" }])
      .subscribe((res) => {
        const total = res["hydra:totalItems"];
        this.totalCount = total;
      });
  }
  changeStatut(id, active: boolean) {
    this.statut.isActive = active ? false : true;
    this.share.updateUserData(this.url, id, this.statut).subscribe(
      (res) => {
        if (
          this.users.length == 1 &&
          this.pagination.current > 1 &&
          this.filters[4].is_active != ""
        ) {
          this.getUsers(this.pagination.prev);
        } else {
          this.getUsers(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
          localStorage.clear();
        }
      }
    );
  }
  totalCount: number;
  getUsers(page: number) {
    this.dataNotFound = false;
    this.isLoading = true;
    this.filters[1].page = page;
    this.share.getEntity(this.url, this.filters).subscribe(
      (res) => {
        this.isLoading = false;
        this.users = res["hydra:member"];
        if (this.users.length == 0) {
          this.dataNotFound = true;
        } else {
          this.dataNotFound = false;
        }
        if (page == 1) {
          this.pagination.prev = 1;
        } else {
          this.pagination.prev = parseInt(
            res["hydra:view"]["hydra:previous"]?.split("=").reverse()[0]
          );
        }
        if (
          page ==
          parseInt(res["hydra:view"]["hydra:last"]?.split("=").reverse()[0])
        ) {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
          );
        } else {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:next"]?.split("=").reverse()[0]
          );
        }
        this.pagination.current = page;
        this.pagination.first = parseInt(
          res["hydra:view"]["hydra:first"]?.split("=").reverse()[0]
        );
        this.pagination.last = parseInt(
          res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
        );
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
          localStorage.clear();
        }
        this.isLoading = false;
      }
    );
  }
  createNewUser() {
    this.isAdding = true;
    if (this.newUser.lastName === null) {
      this.newUser.lastName = "";
    } else if (this.newUser.firstName === null) {
      this.newUser.firstName = "";
    } else if (!this.entityExist && !this.entityEmpty) {
      this.share.addEntity(this.url, this.newUser).subscribe(
        (res) => {
          this.getUsers(this.pagination.current);
          this.countUsers();
          this.newUser = {
            email: null,
            password: Math.random().toString(36).slice(-10),
            username: "Pseudo",
            roles: ["ROLE_USER"],
            lastName: null,
            firstName: null,
            isActive: true,
            confirmedAccount: false,
            language: "fr",
          };
          this.errorAddUser = false;
          this.successAddUser = true;
          this.inputStyle.backgroundColor = "";
          this.isAdding = false;
        },
        (error) => {
          if (error.status == 401) {
            localStorage.clear();
            this.router.navigate(["/login"]);
            this.isAdding = false;
          }
          console.log(error);
          this.successAddUser = false;
          this.errorAddUser = true;
          this.isAdding = false;
        }
      );
    } else {
      this.errorAddUser = true;
      this.isAdding = false;
    }
  }
  resetAddModal() {
    this.newUser = {
      email: null,
      password: Math.random().toString(36).slice(-10),
      username: "Pseudo",
      roles: ["ROLE_USER"],
      lastName: null,
      firstName: null,
      isActive: true,
      confirmedAccount: false,
      language: "fr",
    };
    this.successAddUser = false;
    this.errorAddUser = false;
    this.inputStyle.backgroundColor = "";
    this.entityExist = false;
    this.entityEmpty = false;
  }
  showUpdateModal(user) {
    this.updateUser = {
      id: user.id,
      email: user.email,
      username: user.username,
      lastName: user.last_name,
      firstName: user.first_name,
      isActive: user.is_active,
      roles: user.roles,
    };
  }
  putUser() {
    this.share
      .updateUserData(this.url, this.updateUser.id, this.updateUser)
      .subscribe(
        (res) => {
          this.getUsers(this.pagination.current);
          this.successModifyUser = true;
          this.delay(5000).then((any) => {
            this.successModifyUser = false;
          });
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          } else if (error.status == 422) {
            this.failModifyUser = true;
            this.delay(10000).then((any) => {
              this.failModifyUser = false;
            });
          }
        }
      );
  }
  uniquenessCheck() {
    if (this.newUser.email) {
      this.share
        .getEntity(this.url, [{ email: this.newUser.email }])
        .subscribe((res) => {
          if (this.newUser.email == "") {
            this.inputStyle.backgroundColor = "#fee2e1";
            this.entityEmpty = true;
          } else {
            let exist = false;
            if (res["hydra:member"].length > 0) {
              res["hydra:member"].map((item) => {
                if (item.email === this.newUser.email) {
                  exist = true;
                }
              });
              if (exist) {
                this.inputStyle.backgroundColor = "#fee2e1";
                this.entityExist = true;
                this.entityEmpty = false;
              } else {
                this.inputStyle.backgroundColor = "#dbf2e3";
                this.entityExist = false;
                this.entityEmpty = false;
              }
            } else {
              this.inputStyle.backgroundColor = "#dbf2e3";
              this.entityExist = false;
              this.entityEmpty = false;
            }
          }
        });
    } else {
      this.inputStyle.backgroundColor = "#fee2e1";
    }
  }

  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("")
    );
  }

  checkSyntaxMail() {
    let re =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.syntaxMail = !re.test(this.newUser.email);
  }
}
