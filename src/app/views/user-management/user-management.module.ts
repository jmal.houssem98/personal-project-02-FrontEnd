import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserManagementComponent} from "./user-management.component";
import {UserManagementRoutingModule} from "./user-management-routing.module";
import {FormsModule} from "@angular/forms";
import {TypeGenerauxRoutingModule} from "../type-generaux/type-generaux-routing.module";
import {ModalModule} from "ngx-bootstrap/modal";



@NgModule({
  declarations: [UserManagementComponent],
  imports: [
    CommonModule,
    FormsModule,
    UserManagementRoutingModule,
    ModalModule.forRoot(),
  ]
})
export class UserManagementModule { }
