import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { SharedService } from "../../shared/services/shared.service";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent implements OnInit {
  public formGroup: FormGroup;
  public formUpdateGroup: FormGroup;
  public formPasswordGroup: FormGroup;

  url = "/users";
  localUserData: any;
  profileModified = false;
  errorHappened = false;
  isConfirmed = false;
  inputStyle = {
    backgroundColor: "",
  };
  inputStyle2 = {
    backgroundColor: "",
  };
  entityEmpty = false;
  entityExist = false;
  entityEmpty2 = false;
  entityExist2 = false;
  updatePasswordFailure = false;

  constructor(private service: SharedService, private router: Router) {}
  ngOnInit(): void {
    this.localUserData = JSON.parse(localStorage.getItem("user"));
    this.isConfirmed = this.localUserData.confirmed_account;
    this.initForm();
    this.formGroup.patchValue({
      email: this.localUserData.email,
      username: this.localUserData.username,
      lastName: this.localUserData.last_name,
      firstName: this.localUserData.first_name,
    });
    this.updatePasswordFailure = false;
  }
  resetForm() {
    this.formGroup.patchValue({
      email: this.localUserData.email,
      username: this.localUserData.username,
      lastName: this.localUserData.last_name,
      firstName: this.localUserData.first_name,
    });
  }

  initForm() {
    this.formGroup = new FormGroup({
      email: new FormControl("", [Validators.required]),
      username: new FormControl("", [Validators.required]),
      lastName: new FormControl("", [Validators.required]),
      firstName: new FormControl("", [Validators.required]),
    });
    this.formUpdateGroup = new FormGroup({
      oldPassword: new FormControl("", [Validators.required]),
      newPassword: new FormControl("", [Validators.required]),
      twinPassword: new FormControl("", [Validators.required]),
    });

    this.formPasswordGroup = new FormGroup({
      password: new FormControl("", [Validators.required]),
    });
  }

  updateProcess() {
    const val = this.formGroup.value;

    if (this.formGroup.valid) {
      this.service
        .updateUserData(this.url, this.localUserData.id, val)
        .subscribe(
          (res: any) => {
            this.profileModified = true;
            this.localUserData.email = res.email;
            this.localUserData.username = res.username;
            this.localUserData.last_name = res.last_name;
            this.localUserData.first_name = res.first_name;
            localStorage.removeItem("user");
            localStorage.setItem("user", JSON.stringify(this.localUserData));
            this.delay(10000).then((any) => {
              this.profileModified = false;
            });
          },
          (error) => {
            this.errorHappened = true;
            this.delay(10000).then((any) => {
              this.errorHappened = false;
            });
          }
        );
    } else {
      this.errorHappened = true;
      this.delay(10000).then((any) => {
        this.errorHappened = false;
      });
    }
  }
  uniquenessCheckEmail() {
    this.service
      .getEntity(this.url, [{ email: this.formGroup.value.email }])
      .subscribe((res) => {
        if (!this.formGroup.value.email) {
          this.inputStyle.backgroundColor = "#fee2e1";
          this.entityEmpty = true;
        } else {
          let exist = false;
          if (res["hydra:member"].length > 0) {
            res["hydra:member"].map((item) => {
              if (item.email === this.formGroup.value.email) {
                if (this.formGroup.value.email == this.localUserData.email) {
                  exist = false;
                } else {
                  exist = true;
                }
              }
            });
            if (exist) {
              this.inputStyle.backgroundColor = "#fee2e1";
              console.log("this email exist");
              this.entityExist = true;
              this.entityEmpty = false;
            } else {
              this.entityEmpty = false;
              this.inputStyle.backgroundColor = "#dbf2e3";
              this.entityExist = false;
            }
          } else {
            this.inputStyle.backgroundColor = "#dbf2e3";
            this.entityExist = false;
            this.entityEmpty = false;
          }
        }
      });
  }

  uniquenessCheckUsername() {
    this.service
      .getEntity(this.url, [{ username: this.formGroup.value.username }])
      .subscribe((res) => {
        if (!this.formGroup.value.username) {
          this.inputStyle2.backgroundColor = "#fee2e1";
          this.entityEmpty2 = true;
        } else {
          let exist = false;
          if (res["hydra:member"].length > 0) {
            res["hydra:member"].map((item) => {
              if (item.username === this.formGroup.value.username) {
                if (
                  this.formGroup.value.username == this.localUserData.username
                ) {
                  exist = false;
                } else {
                  exist = true;
                }
              }
            });
            if (exist) {
              this.inputStyle2.backgroundColor = "#fee2e1";
              this.entityExist2 = true;
              this.entityEmpty2 = false;
            } else {
              this.inputStyle2.backgroundColor = "#dbf2e3";
              this.entityEmpty2 = false;
              this.entityExist2 = false;
            }
          } else {
            this.inputStyle2.backgroundColor = "#dbf2e3";
            this.entityExist2 = false;
            this.entityEmpty2 = false;
          }
        }
      });
  }

  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("Notification time")
    );
  }
  correctOldPassword = false;
  firstEssay = true;
  updatePasswordSuccess = false;

  updatePassword() {
    // first part of this code to check old password
    this.service
      .login(this.localUserData.email, this.formUpdateGroup.value.oldPassword)
      .subscribe(
        (res: any) => {
          this.correctOldPassword = true;
          this.firstEssay = false;
        },
        (error) => {
          if (error.status == 401) {
            this.correctOldPassword = false;
            this.firstEssay = false;
          } else {
            alert("Veuillez vérifier votre connexion et réessayer");
          }
        }
      );
    // Second part to modify password
    this.updatePasswordFailure = false;
    this.delay(3500).then((any) => {
      if (this.isTwin && this.isValid && this.correctOldPassword) {
        console.log("perfect conditions");
        this.updatePasswordApi();
        this.updatePasswordFailure = false;
        this.updatePasswordSuccess = true;
      } else {
        console.log("can't continue !!");
        this.updatePasswordFailure = true;
        this.updatePasswordSuccess = false;
      }
    });
  }

  isValid = false;
  firstEssay2 = true;

  checkPasswordValidity() {
    this.firstEssay2 = false;
    this.CheckPassword(this.formUpdateGroup.value.newPassword);
  }

  CheckPassword(inputtxt) {
    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    if (inputtxt.match(passw)) {
      this.isValid = true;
      //return true;
    } else {
      this.isValid = false;
      //return false;
    }
  }

  firstEssay3 = true;
  isTwin = false;
  checkPasswordTwin() {
    this.firstEssay3 = false;
    if (
      this.formUpdateGroup.value.newPassword ==
      this.formUpdateGroup.value.twinPassword
    ) {
      if (this.formUpdateGroup.value.twinPassword == "") {
        this.isTwin = false;
      } else {
        this.isTwin = true;
      }
    } else {
      this.isTwin = false;
    }
  }

  updatePasswordApi() {
    this.formPasswordGroup.patchValue({
      password: this.formUpdateGroup.value.newPassword,
    });
    const val = this.formPasswordGroup.value;
    console.log(val);
    this.service.updateUserData(this.url, this.localUserData.id, val).subscribe(
      (res: any) => {
        //this.profileModified = true;
        //this.localUserData.email = res.email;
      },
      (error) => {}
    );
  }
}
