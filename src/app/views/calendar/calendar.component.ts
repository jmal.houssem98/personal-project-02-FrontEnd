import { Component, OnInit } from "@angular/core";
import { SharedService } from "../../shared/services/shared.service";
import { Router } from "@angular/router";
import { LoaderService } from "../../shared/services/loader.service";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-calendar",
  templateUrl: "./calendar.component.html",
  styleUrls: ["./calendar.component.scss"],
})
export class CalendarComponent implements OnInit {
  client = {
    firstName: "",
    lastName: "",
    country: "",
    town: "",
    email: "",
  };
  clientName = "";
  filterNom = "";
  filterPrenom = "";
  listPays = ["tunisie", "Algerie", "France"];
  listRegion = ["tunis", "Sfax", "Sousse", "Monastir", "Medenine"];
  totalPriceDay = 0;
  productsReserv = [{ model: "a", price: 0 }];
  updateReserv = false;
  isLoading = false;
  dataNotFound = false;
  newReparation = {
    startPeriod: "s",
    endPeriod: "s",
    startDate: null,
    endDate: null,
    client: "",
    status: "Réservation",
    report: "En attente",
    products: ["a"],
  };
  addForm: NgForm;
  addFormClent: NgForm;
  successAddSiteType = false;
  errorAddSiteType = false;
  prodReservDay = [];
  url = "/products";
  products = [];
  listReserv = [];
  monthNames = [
    "janvier",
    "février",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "août",
    "septembre",
    "octobre",
    "novembre",
    "décembre",
  ];
  selectedYeaur = new Date().getFullYear();
  selectedmonth = new Date().getMonth();
  showDatePicker = false;
  yearMonth = this.selectedYeaur;
  dayList = this.getDaysInMonth(this.selectedmonth, this.yearMonth);
  clients = [];
  showAddFormClient = false;
  ReservContrat = false;
  private diffDate = 1;
  private idRev = null;
  private enableContrat = false;
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.getproducts();
  }

  getDaysInMonth(month, year) {
    this.prodReservDay = [];
    const options = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
    };
    let day: any;
    var date = new Date(year, month, 1);
    var days = [];
    while (date.getMonth() === month) {
      day = new Date(date).toLocaleDateString(undefined, options);
      days.push(day.split(" "));
      this.prodReservDay.push({
        client: null,
        reserve: false,
        colspan: 1,
        idRev: null,
      });
      date.setDate(date.getDate() + 1);
    }
    this.getproducts();
    return days;
  }

  changeMonth(m: number) {
    if (m > 11) {
      this.selectedmonth = 0;
      this.yearMonth = this.selectedYeaur + 1;
      this.selectedYeaur = this.selectedYeaur + 1;
      this.showDatePicker = false;
      this.dayList = this.getDaysInMonth(1, this.selectedYeaur);
    } else {
      // @ts-ignore
      if (m < 0) {
        this.selectedmonth = 11;
        this.yearMonth = this.selectedYeaur - 1;
        this.selectedYeaur = this.selectedYeaur - 1;
        this.showDatePicker = false;
        this.dayList = this.getDaysInMonth(11, this.selectedYeaur);
      } else {
        this.selectedmonth = m;
        this.yearMonth = this.selectedYeaur;
        this.showDatePicker = false;
        this.dayList = this.getDaysInMonth(this.selectedmonth, this.yearMonth);
      }
    }
  }
  aujourdhui() {
    this.changeMonth(new Date().getMonth());
  }

  getproducts() {
    this.isLoading = true;
    this.share.getEntity(this.url, []).subscribe(
      (res) => {
        this.products = res["hydra:member"];
        this.isLoading = false;
        // this.getReserv()
        this.getReservations();
        // this.listReserv=
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(["/login"]);
        }
        console.log(error);
      }
    );
  }
  getReservations() {
    this.isLoading = true;
    this.listReserv = [];
    let prods = [];
    for (let i = 0; i < this.products.length; i++) {
      prods.push(this.products[i]);
    }
    prods.map(
      prod=>{
        var ligne=[]
        for (let i=0;i<this.prodReservDay.length;i++){
          ligne.push({...this.prodReservDay[i]})
        }
        if (prod.reservations.length>0) {
          prod.reservations.map(rev => {
            let a = parseInt(rev.startDate.split('T')[0].split('-')[2])
            let b = parseInt(rev.endDate.split('T')[0].split('-')[2])
            let x = parseInt(rev.startDate.split('T')[0].split('-')[1]) - 1
            let y = parseInt(rev.endDate.split('T')[0].split('-')[1]) - 1
            let c=b-a
            if (x == this.selectedmonth) {
              let z=0
              for (let i=0;i<a-1;i++){
                z=z+ligne[i].colspan
              }
              a=2*a-z-1
              b=a+c
              ligne[a - 1].client = rev.client.firstName + ' ' + rev.client.lastName
              ligne[a - 1].reserve = true
              ligne[a - 1].idRev = rev.id
              ligne[a - 1].status=rev.status
              if (y == this.selectedmonth) {
                ligne[a - 1].colspan = b - a + 1
                ligne.splice(a, b - a)
              } else {
                ligne[a - 1].colspan = this.dayList.length - a + 1
                ligne.splice(a, this.dayList.length - a)
              }
            } else if (x < this.selectedmonth) {
              if (y == this.selectedmonth) {
                ligne[0].client = rev.client.firstName + ' ' + rev.client.lastName
                ligne[0].reserve = true
                ligne[0].colspan = b
                ligne[0].idRev = rev.id
                ligne[0].status = rev.status
                ligne.splice(1, b)
              } else if (y > this.selectedmonth) {
                ligne[0].client = rev.client.firstName + ' ' + rev.client.lastName
                ligne[0].reserve = true
                ligne[0].colspan = this.dayList.length
                ligne[0].idRev = rev.id
                ligne[0].status = rev.status
                ligne.splice(1, this.dayList.length - 1)
              }
            }
        });
      }
      this.listReserv.push(ligne);
    });
    this.isLoading = false;
  }
  resetAddModalClent() {
    this.client = {
      firstName: "",
      lastName: "",
      country: "",
      town: "",
      email: "",
    };
    this.showAddFormClient = false;
    if (this.addFormClent) {
      Object.keys(this.addFormClent.controls).forEach((key) => {
        const control = this.addFormClent.controls[key];
        control.markAsPristine();
        control.markAsUntouched();
      });
    }
  }
  resetAddModal() {
    this.clientName = "";
    this.productsReserv = [{ model: "a", price: 0 }];
    this.newReparation.products = [];
    if (this.addForm) {
      Object.keys(this.addForm.controls).forEach((key) => {
        const control = this.addForm.controls[key];
        control.markAsPristine();
        control.markAsUntouched();
      });
    }
  }
  reservation(materielIndex: number, j: number, idRev: any) {
    this.newReparation.products = ["a"];
    if (idRev) {
      this.idRev = idRev;
      this.share.getOneEntity("/reservations", idRev).subscribe(
        (res) => {
          this.newReparation.client = res["client"]["@id"];
          this.clientName =
            res["client"]["firstName"] + " " + res["client"]["lastName"];
          this.newReparation.endDate = res["endDate"].split("T")[0];
          this.newReparation.startDate = res["startDate"].split("T")[0];
          this.productsReserv = res["products"];
          this.enableContrat =
            new Date() >= new Date(this.newReparation.startDate);
          this.refrechTotal();
          for (let i = 0; i < res["products"].length; i++) {
            this.newReparation.products[i] = res["products"][i]["@id"];
          }
          this.newReparation.report = "Validé";
          this.newReparation.status = res["status"];
          if (res["status"] == "Contrat") {
            this.ReservContrat = true;
          } else {
            this.ReservContrat = false;
          }
        },
        (error) => {
          if (error.status == 401) {
            localStorage.clear();
            this.router.navigate(["/login"]);
          }
          console.log(error);
        }
      );
      this.updateReserv = idRev;
    } else {
      this.ReservContrat = false;
      this.idRev = null;
      this.updateReserv = null;
      this.newReparation.client = "";
      this.newReparation.status = "Réservation";
      this.newReparation.report = "En attente";

      let material = this.products[materielIndex];
      this.newReparation.products[0] = material["@id"];
      this.productsReserv[0] = material;
      let day = this.dayList[j];
      let d = 0;
      for (let i = 0; i < j + 1; i++) {
        d = d + this.listReserv[materielIndex][i].colspan;
      }
      let m = this.monthNames.indexOf(day[2]) + 1;

      if (m > 9) {
        this.newReparation.startDate = day[3] + "-" + m;
      } else {
        this.newReparation.startDate = day[3] + "-0" + m;
      }
      if (d > 9) {
        this.newReparation.startDate = this.newReparation.startDate + "-" + d;
      } else {
        this.newReparation.startDate = this.newReparation.startDate + "-0" + d;
      }
      this.newReparation.endDate = this.newReparation.startDate;
    }
    this.enableContrat = new Date() >= new Date(this.newReparation.startDate);
    this.refrechTotal();
  }
  getClients() {
    this.share.getEntity("/clients", []).subscribe(
      (res) => {
        this.clients = res["hydra:member"];
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(["/login"]);
        }
        console.log(error);
      }
    );
  }

  ajouterReserv() {
    if (this.updateReserv) {
      this.share
        .updateEntity("/reservations", this.updateReserv, this.newReparation)
        .subscribe(
          (res) => {
            this.getproducts();
          },
          (error) => {
            if (error.status == 401) {
              localStorage.clear();
              this.router.navigate(["/login"]);
            }
            console.log(error);
          }
        );
    } else {
      this.share.addEntity("/reservations", this.newReparation).subscribe(
        (res) => {
          this.getproducts();
        },
        (error) => {
          if (error.status == 401) {
            localStorage.clear();
            this.router.navigate(["/login"]);
          }
          console.log(error);
        }
      );
    }
    this.resetAddModal();
  }

  addProd(i) {
    // @ts-ignore
    this.productsReserv[i + 1] = {};
    this.newReparation.products[i + 1] = "a";
  }

  delProd(i: number) {
    this.newReparation.products.splice(i, 1);
    this.productsReserv.splice(i, 1);
    this.refrechTotal();
  }

  changeStatut() {
    this.showAddFormClient = !this.showAddFormClient;
  }

  ajouterClient() {
    this.share.addEntity("/clients", this.client).subscribe(
      (res) => {
        this.newReparation.client = res["@id"];
        this.clientName = res["firstName"] + " " + res["lastName"];
      },
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(["/login"]);
        }
        console.log(error);
      }
    );
  }

  selectClient(usr: any) {
    this.newReparation.client = usr["@id"];
    this.clientName = usr["firstName"] + " " + usr["lastName"];
  }

  confirmProd(event, i: number) {
    this.productsReserv[i] =
      this.products[event.target.value.split(":")[0] - 1];
    this.refrechTotal();
  }
  effacerClient() {
    this.newReparation.client = "";
    this.clientName = "";
  }
  filterClients() {
    this.share
      .getEntity("/clients", [
        { firstName: this.filterPrenom },
        { lastName: this.filterNom },
      ])
      .subscribe(
        (res) => {
          this.clients = res["hydra:member"];
        },
        (error) => {
          if (error.status == 401) {
            localStorage.clear();
            this.router.navigate(["/login"]);
          }
          console.log(error);
        }
      );
  }
  refrechTotal() {
    this.refrechDate();
    this.totalPriceDay = 0;
    this.productsReserv.map((prod) => {
      this.totalPriceDay = this.totalPriceDay + prod.price;
    });
  }
  refrechDate() {
    let x = new Date(this.newReparation.startDate);
    let y = new Date(this.newReparation.endDate);
    let z = (y.getTime() - x.getTime()) / (1000 * 3600 * 24);
    this.diffDate = z + 1;
  }

  changeDateDebut() {
    if (this.newReparation.startDate > this.newReparation.endDate) {
      this.newReparation.endDate = this.newReparation.startDate;
    }
    this.refrechDate();
  }

  deleteRev() {
    this.share.deleteEntity("reservations", this.idRev).subscribe(
      (res) => {},
      (error) => {
        if (error.status == 401) {
          localStorage.clear();
          this.router.navigate(["/login"]);
        }
        console.log(error);
      }
    );
    this.getproducts();
  }
}
