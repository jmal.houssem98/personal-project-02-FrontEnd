import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { SharedService } from "../../shared/services/shared.service";
import { LoaderService } from "../../shared/services/loader.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  public formGroup: FormGroup;
  hide: false;

  constructor(
    private service: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}
  token: any;
  res: any;
  error: string = null;
  showForm = true;

  ngOnInit(): void {
    localStorage.clear();
    this.initForm();
    this.hide = false;
    this.showForm = true;
  }

  initForm() {
    this.formGroup = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required]),
    });
  }

  loginProcess() {
    const val = this.formGroup.value;
    if (this.formGroup.valid) {
      this.showForm = false;
      this.service.login(val.username, val.password).subscribe(
        (res: any) => {
          if (res.user.is_active) {
            if (res.user.confirmed_account) {
              localStorage.setItem("token", res.token);
              localStorage.setItem("user", JSON.stringify(res.user));
              this.router.navigate(["/dashboard"]);
            } else {
              localStorage.setItem("token", res.token);
              localStorage.setItem("idUser", JSON.stringify(res.user.id));
              this.router.navigate(["/resetPassword"]);
            }
          } else {
            this.showForm = true;
            this.error =
              "Votre compte n'est pas activé.    Veuillez contactez votre administrateur pour obtenir plus d'informations.";
          }
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          }
          this.showForm = true;
          this.error = "Le mail ou mot de passe est incorect !";
        }
      );
    } else {
      this.error = "Le mail ou mot de passe est incorect !";
    }
  }
  getToekn() {
    return localStorage.getItem("token");
  }
}
