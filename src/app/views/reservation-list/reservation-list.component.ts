import { Component, OnInit, ViewChild } from "@angular/core";
import { SharedService } from "../../shared/services/shared.service";
import { Router } from "@angular/router";
import { LoaderService } from "../../shared/services/loader.service";

import { jsPDF } from "jspdf";

@Component({
  selector: "app-reservation-list",
  templateUrl: "./reservation-list.component.html",
  styleUrls: ["./reservation-list.component.scss"],
})
export class ReservationListComponent implements OnInit {
  url = "/reservations";
  inputStyle = {
    backgroundColor: "",
  };
  isLoading = false;
  dataNotFound = false;
  emptyPick = false;

  entityExist = false;
  entityEmpty = false;
  user = {
    isAdmin: true,
  };
  filters = [
    { id: null },
    { page: 1 },
    { itemsPerPage: "10" },
    { client: "" },
    { status: "" },
    { products: "" },
  ];

  filterId = false;
  filterElement = false;
  filterAccommodationSource = false;
  filterAccommodationTarget = false;
  filterCampingSource = false;
  filterCampingTarget = false;

  newtSiteType = {
    element: "",
    accommodationSource: "",
    accommodationTarget: "",
    campingSource: "",
    campingTarget: "",
    startDate: null,
    endDate: null,
  };
  updateSiteType = {
    id: null,
    element: "",
    accommodationSource: "",
    accommodationTarget: "",
    campingSource: "",
    campingTarget: "",
    startDate: null,
    endDate: null,
  };
  successAddSiteType = false;
  errorAddSiteType = false;
  successModifySiteType = false;
  failModifySiteType = false;
  siteTypes = [];
  rentalTypes = [];

  statut = {
    isActive: null,
  };
  loading = true;
  pagination = {
    first: 1,
    prev: 1,
    current: 1,
    next: 1,
    last: 1,
  };
  constructor(
    private share: SharedService,
    private router: Router,
    public loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.user.isAdmin =
      this.share.getUserRole() === "ROLE_ADMIN" ? true : false;
    this.getSiteTypes(1);
    this.share
      .getEntity(this.url, [{ page: 1 }, { itemsPerPage: "1" }])
      .subscribe((res) => {
        const total = res["hydra:totalItems"];
        this.totalCount = total;
      });
    this.getElements();
    this.getAccomodations();
    this.getSites();
  }
  changeStatut(id, active: boolean) {
    this.statut.isActive = active ? false : true;
    this.share.updateEntity(this.url, id, this.statut).subscribe(
      (res) => {
        if (this.siteTypes.length == 1 && this.pagination.current > 1) {
          this.getSiteTypes(this.pagination.prev);
        } else {
          this.getSiteTypes(this.pagination.current);
        }
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
      }
    );
  }
  totalCount: number;
  getSiteTypes(page: number) {
    this.dataNotFound = false;
    this.isLoading = true;
    this.filters[1].page = page;
    this.share.getEntity(this.url, this.filters).subscribe(
      (res) => {
        this.isLoading = false;
        this.siteTypes = res["hydra:member"];
        if (this.siteTypes.length == 0) {
          this.dataNotFound = true;
        } else {
          this.dataNotFound = false;
        }
        if (page == 1) {
          this.pagination.prev = 1;
        } else {
          this.pagination.prev = parseInt(
            res["hydra:view"]["hydra:previous"]?.split("=").reverse()[0]
          );
        }
        if (
          page ==
          parseInt(res["hydra:view"]["hydra:last"]?.split("=").reverse()[0])
        ) {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
          );
        } else {
          this.pagination.next = parseInt(
            res["hydra:view"]["hydra:next"]?.split("=").reverse()[0]
          );
        }
        this.pagination.current = page;
        this.pagination.first = parseInt(
          res["hydra:view"]["hydra:first"]?.split("=").reverse()[0]
        );
        this.pagination.last = parseInt(
          res["hydra:view"]["hydra:last"]?.split("=").reverse()[0]
        );
      },
      (error) => {
        if (error.status == 401) {
          this.router.navigate(["/login"]);
        }
        this.isLoading = false;
      }
    );
  }
  addNewSiteType() {
    if (!this.entityExist && !this.entityEmpty) {
      this.share.addEntity(this.url, this.newtSiteType).subscribe(
        (res) => {
          this.emptyPick = false;
          this.getSiteTypes(this.pagination.current);
          this.newtSiteType = {
            element: "",
            accommodationSource: "",
            accommodationTarget: "",
            campingSource: "",
            campingTarget: "",
            startDate: null,
            endDate: null,
          };
          this.errorAddSiteType = false;
          this.successAddSiteType = true;
        },
        (error) => {
          if (error.status == 401) {
            this.router.navigate(["/login"]);
          }
          this.successAddSiteType = false;
          this.errorAddSiteType = true;
        }
      );
    }
  }
  resetAddModal() {
    this.emptyPickElement = false;
    this.emptyPickAccommodationSource = false;
    this.emptyPickAccommodationTarget = false;
    this.emptyPickCampingSource = false;
    this.emptyPickCampingTarget = false;
    this.emptyDateStartDate = false;
    this.emptyDateEndDate = false;

    this.newtSiteType = {
      element: "",
      accommodationSource: "",
      accommodationTarget: "",
      campingSource: "",
      campingTarget: "",
      startDate: null,
      endDate: null,
    };
    this.successAddSiteType = false;
    this.errorAddSiteType = false;
    this.inputStyle.backgroundColor = "";
    this.entityExist = false;
    this.entityEmpty = false;
  }
  showUpdateModal(siteType) {
    this.getElements();
    this.updateSiteType = {
      id: siteType.id,
      element: siteType.element ? siteType.element["@id"] : "",
      accommodationSource: siteType.accommodation_source
        ? siteType.accommodation_source["@id"]
        : "",
      accommodationTarget: siteType.accommodation_target
        ? siteType.accommodation_target["@id"]
        : "",
      campingSource: siteType.camping_source
        ? siteType.camping_source["@id"]
        : "",
      campingTarget: siteType.camping_target
        ? siteType.camping_target["@id"]
        : "",
      startDate: siteType.start_date.split("T")[0],
      endDate: siteType.end_date.split("T")[0],
    };
  }

  async delay(ms: number) {
    await new Promise<void>((resolve) => setTimeout(() => resolve(), ms)).then(
      () => console.log("fired")
    );
  }
  elements = [];
  accommodations = [];
  sites = [];
  getElements() {
    this.share.getEntity("/elements", []).subscribe((result) => {
      this.elements = result["hydra:member"];
    });
  }
  getAccomodations() {
    this.share.getEntity("/accommodations", []).subscribe((result) => {
      this.accommodations = result["hydra:member"];
    });
  }
  getSites() {
    this.share.getEntity("/sites", []).subscribe((result) => {
      this.sites = result["hydra:member"];
    });
  }

  emptyPickElement = false;
  emptyPickAccommodationSource = false;
  emptyPickAccommodationTarget = false;
  emptyPickCampingSource = false;
  emptyPickCampingTarget = false;
  emptyDateStartDate = false;
  emptyDateEndDate = false;

  checkNoEmptyFields() {
    if (!this.newtSiteType.element) {
      this.emptyPickElement = true;
    } else {
      this.emptyPickElement = false;
    }

    if (!this.newtSiteType.accommodationTarget) {
      this.emptyPickAccommodationTarget = true;
    } else {
      this.emptyPickAccommodationTarget = false;
    }

    if (!this.newtSiteType.accommodationSource) {
      this.emptyPickAccommodationSource = true;
    } else {
      this.emptyPickAccommodationSource = false;
    }

    if (!this.newtSiteType.campingSource) {
      this.emptyPickCampingSource = true;
    } else {
      this.emptyPickCampingSource = false;
    }

    if (!this.newtSiteType.campingTarget) {
      this.emptyPickCampingTarget = true;
    } else {
      this.emptyPickCampingTarget = false;
    }

    if (!this.newtSiteType.startDate) {
      this.emptyDateStartDate = true;
    } else {
      this.emptyDateStartDate = false;
    }

    if (!this.newtSiteType.endDate) {
      this.emptyDateEndDate = true;
    } else {
      this.emptyDateEndDate = false;
    }
  }
  // PDF functions
  reservation: any;
  i = 230;
  total = 0;
  date: any;

  generatePdf(index: any) {
    this.date = new Date().toLocaleString();
    this.total = 0;
    this.i++;
    this.reservation = this.siteTypes[index];
    this.makePDF();
  }

  makePDF() {
    let pdf = new jsPDF();
    pdf.text("Talan Tunisie", 165, 10);

    pdf.text(
      "Rapport de " +
        String(this.reservation.status) +
        " n° : " +
        String(this.i),
      10,
      15
    );
    pdf.text("Statut : " + String(this.reservation.report), 15, 25);
    pdf.text("Date de création : " + String(this.date), 15, 35);

    pdf.text("Client : ", 10, 50);
    pdf.text(
      "Nom et prénom : " +
        String(this.reservation.client.firstName) +
        " " +
        String(this.reservation.client.lastName),
      14,
      60
    );
    pdf.text(
      "Adresse : Rue 42, " +
        String(this.reservation.client.town) +
        ", " +
        String(this.reservation.client.country),
      14,
      70
    );
    pdf.text("Numéro Téléphone : (+216) 53 198 480", 14, 80);
    pdf.text("Email : " + String(this.reservation.client.Email), 14, 90);

    pdf.text("* Informations à propos réservation: ", 10, 110);
    pdf.text(
      "Date de début :  " + String(this.reservation.startDate.slice(0, 10)),
      14,
      120
    );
    pdf.text(
      "Date de fin      :  " + String(this.reservation.endDate.slice(0, 10)),
      14,
      130
    );
    let line = 170;
    for (let i = 0; i < this.reservation.products.length; i++) {
      pdf.text("Produits : ", 14, 150);
      pdf.text("Modéle", 25, 160);
      pdf.text("Prix", 140, 160);

      pdf.text(
        String(this.reservation.products[i].serialNumber) +
          "-" +
          String(this.reservation.products[i].model),
        25,
        line
      );
      pdf.text(String(this.reservation.products[i].price), 140, line);
      line += 10;
      this.total += this.reservation.products[i].price;
    }

    pdf.text("Total : " + String(this.total) + " €", 140, line + 30);
    pdf.text("Signature", 150, 260);

    pdf.save("report.pdf");
  }
}
