import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {SharedService} from "../../shared/services/shared.service";
import {error} from "protractor";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  url = "/users";
  user:any
  newPass:""
  confirmPass:""
  passMatch=true
  constructor(
    private router: Router,
    private share: SharedService,
  ) {
  }

  ngOnInit(): void {
    if (localStorage.getItem("token")&&localStorage.getItem("idUser")){
      this.share.getUser().subscribe((res:any)=>{
        if (res.id.toString()===localStorage.getItem("idUser").toString()&&!res.confirmed_account) {
          this.user = res
          console.log("user", this.user)
        }else {
          localStorage.clear()
          this.router.navigate(["/login"]);
        }
      },error=>{
        localStorage.clear()
        this.router.navigate(["/login"]);
      })
    }else {
      localStorage.clear()
      this.router.navigate(["/login"]);
    }
  }
  changePass(){
    if (this.newPass.length>0&&this.passMatch===true){
      this.share
        .updateEntity(this.url, this.user.id, {password:this.newPass,confirmedAccount: true})
        .subscribe(
          (res) => {
            localStorage.clear()
            this.router.navigate(["/login"]);
          },
          (error) => {
            console.log("error",error)
          }
        );
    }
  }

  checkPassMatch() {
    if (this.newPass===this.confirmPass){
      this.passMatch=true
    }else {
      this.passMatch=false
    }
  }
}
