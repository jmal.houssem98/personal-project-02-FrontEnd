import { Injectable, Injector } from "@angular/core";
import { catchError, finalize } from "rxjs/operators";
import { Observable } from "rxjs";

import { SharedService } from "../../shared/services/shared.service";
import { LoaderService } from "../../shared/services/loader.service";
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(
    private injector: Injector,
    public loaderService: LoaderService
  ) {}
  intercept(req: any, next: any) {
    this.loaderService.isLoading.next(true);

    let sharedService = this.injector.get(SharedService);
    let tokeniezdRek = req.clone({
      setHeaders: {
        Authorization: `Bearer ${sharedService.getToken()}`,
      },
    });
    return next.handle(tokeniezdRek).pipe(
      finalize(() => {
        this.loaderService.isLoading.next(false);
      })
    );
  }
}
