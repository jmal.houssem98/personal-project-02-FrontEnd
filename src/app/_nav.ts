import { INavData } from "@coreui/angular";

export const navItems: INavData[] = [
  {
    name: "Tableau de bord",
    url: "/dashboard",
    icon: "icon-home",
  },
  {
    name: "Types généraux",
    url: "/type-généraux",
    icon: "cil-equalizer",
    children: [
      {
        name: "Type de site",
        url: "/type-généraux/type-de-site",
      },
      {
        name: "Type d'hébergement",
        url: "/type-généraux/type-d'hébergement",
      },
      {
        name: "Type de locatif",
        url: "/type-généraux/type-de-locatif",
      },
      {
        name: "Type de réparation",
        url: "/type-généraux/type-de-reparation",
      },
      {
        name: "Type d'installation",
        url: "/type-généraux/type-d-installation",
      },
      {
        name: "Type d'élément",
        url: "/type-généraux/type-d-element",
      },
      {
        name: "Type de lieu",
        url: "/type-généraux/type-de-lieu",
      },
      {
        name: "Propriétaire",
        url: "/type-généraux/propriétaire",
      },
      {
        name: "Regroupements",
        url: "/type-généraux/regroupements",
      },
      {
        name: "État d'éléments",
        url: "/type-généraux/etat-d-elements",
      },
      {
        name: "État d'hébergements",
        url: "/type-généraux/état-d'hébergements",
      },
      {
        name: "Fournisseurs de toiles",
        url: "/type-généraux/fournisseurs-de-toiles",
      },
      {
        name: "Confectionneurs",
        url: "/type-généraux/confectionneurs",
      },
      {
        name: "Groupe de site",
        url: "/type-généraux/groupe-de-site",
      },
    ],
  },
  //Données métiers ici
  {
    name: "données métiers",
    url: "/données-métiers",
    icon: "icon-layers",
    children: [
      {
        name: "Sites",
        url: "/données-métiers/site",
      },
      {
        name: "Hébergements",
        url: "/données-métiers/hébergements",
      },
      {
        name: "Lieux",
        url: "/données-métiers/lieux",
      },
      {
        name: "Gammes",
        url: "/données-métiers/gammes",
      },
      {
        name: "Versions",
        url: "/données-métiers/versions",
      },
      {
        name: "Tarifications",
        url: "/données-métiers/tarification",
      },

      {
        name: "Éléments",
        url: "/données-métiers/element",
      },
      {
        name: "Prêts",
        url: "/données-métiers/pret",
      },
      {
        name: "Réparations",
        url: "/données-métiers/reparation",
      },
    ],
  } /*
  {
    name: "location de vélos",
    url: "/calendar",
    icon: "icon-directions",
    class: "mt-auto",
    variant: "success",
    attributes: { target: "_self", rel: "noopener" },
  },
  
  {
    title: true,
    name: "Theme",
  },
  {
    name: "Colors",
    url: "/theme/colors",
    icon: "icon-drop",
  },
  {
    name: "Typography",
    url: "/theme/typography",
    icon: "icon-pencil",
  },
  {
    title: true,
    name: "Components",
  },
  {
    name: "Base",
    url: "/base",
    icon: "icon-puzzle",
    children: [
      {
        name: "Cards",
        url: "/base/cards",
        icon: "icon-puzzle",
      },
      {
        name: "Carousels",
        url: "/base/carousels",
        icon: "icon-puzzle",
      },
      {
        name: "Collapses",
        url: "/base/collapses",
        icon: "icon-puzzle",
      },
      {
        name: "Forms",
        url: "/base/forms",
        icon: "icon-puzzle",
      },
      {
        name: "Navbars",
        url: "/base/navbars",
        icon: "icon-puzzle",
      },
      {
        name: "Pagination",
        url: "/base/paginations",
        icon: "icon-puzzle",
      },
      {
        name: "Popovers",
        url: "/base/popovers",
        icon: "icon-puzzle",
      },
      {
        name: "Progress",
        url: "/base/progress",
        icon: "icon-puzzle",
      },
      {
        name: "Switches",
        url: "/base/switches",
        icon: "icon-puzzle",
      },
      {
        name: "Tables",
        url: "/base/tables",
        icon: "icon-puzzle",
      },
      {
        name: "Tabs",
        url: "/base/tabs",
        icon: "icon-puzzle",
      },
      {
        name: "Tooltips",
        url: "/base/tooltips",
        icon: "icon-puzzle",
      },
    ],
  },
  {
    name: "Buttons",
    url: "/buttons",
    icon: "icon-cursor",
    children: [
      {
        name: "Buttons",
        url: "/buttons/buttons",
        icon: "icon-cursor",
      },
      {
        name: "Dropdowns",
        url: "/buttons/dropdowns",
        icon: "icon-cursor",
      },
      {
        name: "Brand Buttons",
        url: "/buttons/brand-buttons",
        icon: "icon-cursor",
      },
    ],
  },
  {
    name: "Charts",
    url: "/charts",
    icon: "icon-pie-chart",
  },
  {
    name: "Icons",
    url: "/icons",
    icon: "icon-star",
    children: [
      {
        name: "CoreUI Icons",
        url: "/icons/coreui-icons",
        icon: "icon-star",
        badge: {
          variant: "success",
          text: "NEW",
        },
      },
      {
        name: "Flags",
        url: "/icons/flags",
        icon: "icon-star",
      },
      {
        name: "Font Awesome",
        url: "/icons/font-awesome",
        icon: "icon-star",
        badge: {
          variant: "secondary",
          text: "4.7",
        },
      },
      {
        name: "Simple Line Icons",
        url: "/icons/simple-line-icons",
        icon: "icon-star",
      },
    ],
  },
  {
    name: "Notifications",
    url: "/notifications",
    icon: "icon-bell",
    children: [
      {
        name: "Alerts",
        url: "/notifications/alerts",
        icon: "icon-bell",
      },
      {
        name: "Badges",
        url: "/notifications/badges",
        icon: "icon-bell",
      },
      {
        name: "Modals",
        url: "/notifications/modals",
        icon: "icon-bell",
      },
    ],
  },
  {
    name: "Widgets",
    url: "/widgets",
    icon: "icon-calculator",
    badge: {
      variant: "info",
      text: "NEW",
    },
  },
  {
    divider: true,
  },
  {
    title: true,
    name: "Extras",
  },
  {
    name: "Pages",
    url: "/pages",
    icon: "icon-star",
    children: [
      {
        name: "Login",
        url: "/login",
        icon: "icon-star",
      },
      {
        name: "Register",
        url: "/register",
        icon: "icon-star",
      },
      {
        name: "Error 404",
        url: "/404",
        icon: "icon-star",
      },
      {
        name: "Error 500",
        url: "/500",
        icon: "icon-star",
      },
    ],
  },
  {
    name: "Disabled",
    url: "/dashboard",
    icon: "icon-ban",
    badge: {
      variant: "secondary",
      text: "NEW",
    },
    attributes: { disabled: true },
  },*/,
];
