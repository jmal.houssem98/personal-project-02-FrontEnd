import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./core/authentication/auth.guard";

// Import Containers
import { DefaultLayoutComponent } from "./containers";

import { P404Component } from "./views/error/404.component";
import { P500Component } from "./views/error/500.component";
import { LoginComponent } from "./views/login/login.component";
import { RegisterComponent } from "./views/register/register.component";

import { ResetPasswordComponent } from "./views/reset-password/reset-password.component";
import { ProfileComponent } from "./views/profile/profile.component";
import { SettingsComponent } from "./views/settings/settings.component";
import { CalendarComponent } from "./views/calendar/calendar.component";
import { ReservationListComponent } from "./views/reservation-list/reservation-list.component";

export const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },
  {
    path: "404",
    component: P404Component,
    data: {
      title: "Page 404",
    },
  },
  {
    path: "500",
    component: P500Component,
    data: {
      title: "Page 500",
    },
  },
  {
    path: "login",
    component: LoginComponent,
    data: {
      title: "Login Page",
    },
  },
  {
    path: "resetPassword",
    component: ResetPasswordComponent,
    data: {
      title: "reset password Page",
    },
  },
  {
    path: "register",
    component: RegisterComponent,

    data: {
      title: "Register Page",
    },
    canActivate: [AuthGuard],
  },
  {
    path: "",
    component: DefaultLayoutComponent,
    data: {
      title: "Accueil",
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: "gestion-des-utilisateurs",
        loadChildren: () =>
          import("./views/user-management/user-management.module").then(
            (m) => m.UserManagementModule
          ),
      },
      {
        path: "type-généraux",
        loadChildren: () =>
          import("./views/type-generaux/type-generaux.module").then(
            (m) => m.TypeGenerauxModule
          ),
      },
      {
        path: "données-métiers",
        loadChildren: () =>
          import("./views/donnees-metiers/donnees-metiers.module").then(
            (m) => m.DonneesMetiersModule
          ),
      },
      {
        path: "profile",
        component: ProfileComponent,
        data: {
          title: "Profile",
        },
      },
      {
        path: "settings",
        component: SettingsComponent,
        data: {
          title: "Settings",
        },
      },
      {
        path: "calendar",
        component: CalendarComponent,
        data: {
          title: "Calendrier",
        },
      },
      {
        path: "reservation-list",
        component: ReservationListComponent,
        data: {
          title: "Liste des locations",
        },
      },
      {
        path: "base",
        loadChildren: () =>
          import("./views/base/base.module").then((m) => m.BaseModule),
      },
      {
        path: "buttons",
        loadChildren: () =>
          import("./views/buttons/buttons.module").then((m) => m.ButtonsModule),
      },
      {
        path: "charts",
        loadChildren: () =>
          import("./views/chartjs/chartjs.module").then((m) => m.ChartJSModule),
      },
      {
        path: "dashboard",
        loadChildren: () =>
          import("./views/dashboard/dashboard.module").then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: "icons",
        loadChildren: () =>
          import("./views/icons/icons.module").then((m) => m.IconsModule),
      },
      {
        path: "notifications",
        loadChildren: () =>
          import("./views/notifications/notifications.module").then(
            (m) => m.NotificationsModule
          ),
      },
      {
        path: "theme",
        loadChildren: () =>
          import("./views/theme/theme.module").then((m) => m.ThemeModule),
      },
      {
        path: "widgets",
        loadChildren: () =>
          import("./views/widgets/widgets.module").then((m) => m.WidgetsModule),
      },
    ],
  },
  { path: "**", redirectTo: "/dashboard" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: "legacy" })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
